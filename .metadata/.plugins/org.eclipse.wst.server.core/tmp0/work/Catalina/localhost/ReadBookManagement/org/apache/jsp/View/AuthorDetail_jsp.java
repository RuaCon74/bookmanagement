/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/8.5.81
 * Generated at: 2022-08-15 15:10:12 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.View;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class AuthorDetail_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent,
                 org.apache.jasper.runtime.JspSourceImports {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.HashMap<java.lang.String,java.lang.Long>(2);
    _jspx_dependants.put("jar:file:/D:/bai%20tap/thuc%20tap/Git_Project/bookmanagement/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/ReadBookManagement/WEB-INF/lib/jakarta.servlet.jsp.jstl-1.2.6.jar!/META-INF/c.tld", Long.valueOf(1548321666000L));
    _jspx_dependants.put("/WEB-INF/lib/jakarta.servlet.jsp.jstl-1.2.6.jar", Long.valueOf(1658721832438L));
  }

  private static final java.util.Set<java.lang.String> _jspx_imports_packages;

  private static final java.util.Set<java.lang.String> _jspx_imports_classes;

  static {
    _jspx_imports_packages = new java.util.HashSet<>();
    _jspx_imports_packages.add("javax.servlet");
    _jspx_imports_packages.add("javax.servlet.http");
    _jspx_imports_packages.add("javax.servlet.jsp");
    _jspx_imports_classes = null;
  }

  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fc_005fforEach_0026_005fvar_005fitems;

  private volatile javax.el.ExpressionFactory _el_expressionfactory;
  private volatile org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public java.util.Set<java.lang.String> getPackageImports() {
    return _jspx_imports_packages;
  }

  public java.util.Set<java.lang.String> getClassImports() {
    return _jspx_imports_classes;
  }

  public javax.el.ExpressionFactory _jsp_getExpressionFactory() {
    if (_el_expressionfactory == null) {
      synchronized (this) {
        if (_el_expressionfactory == null) {
          _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
        }
      }
    }
    return _el_expressionfactory;
  }

  public org.apache.tomcat.InstanceManager _jsp_getInstanceManager() {
    if (_jsp_instancemanager == null) {
      synchronized (this) {
        if (_jsp_instancemanager == null) {
          _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
        }
      }
    }
    return _jsp_instancemanager;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005fc_005fforEach_0026_005fvar_005fitems = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005fc_005fforEach_0026_005fvar_005fitems.release();
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
      throws java.io.IOException, javax.servlet.ServletException {

    final java.lang.String _jspx_method = request.getMethod();
    if (!"GET".equals(_jspx_method) && !"POST".equals(_jspx_method) && !"HEAD".equals(_jspx_method) && !javax.servlet.DispatcherType.ERROR.equals(request.getDispatcherType())) {
      response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "JSPs only permit GET, POST or HEAD. Jasper also permits OPTIONS");
      return;
    }

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<!DOCTYPE html>\r\n");
      out.write("\r\n");
      out.write("<html lang=\"en\">\r\n");
      out.write("<head>\r\n");
      out.write("<meta charset=\"utf-8\">\r\n");
      out.write("<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n");
      out.write("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n");
      out.write("<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->\r\n");
      out.write("<title>Book Library - Book Guide Author, Publication and Store</title>\r\n");
      out.write("<!-- CUSTOM STYLE -->\r\n");
      out.write("<link href=\"Component/css/style.css\" rel=\"stylesheet\">\r\n");
      out.write("<!-- THEME TYPO -->\r\n");
      out.write("<link href=\"Component/css/themetypo.css\" rel=\"stylesheet\">\r\n");
      out.write("<!-- SHORTCODES -->\r\n");
      out.write("<link href=\"Component/css/shortcode.css\" rel=\"stylesheet\">\r\n");
      out.write("<!-- BOOTSTRAP -->\r\n");
      out.write("<link href=\"Component/css/bootstrap.css\" rel=\"stylesheet\">\r\n");
      out.write("<!-- COLOR FILE -->\r\n");
      out.write("<link href=\"Component/css/color.css\" rel=\"stylesheet\">\r\n");
      out.write("<!-- FONT AWESOME -->\r\n");
      out.write("<link href=\"Component/css/font-awesome.min.css\" rel=\"stylesheet\">\r\n");
      out.write("<!-- BX SLIDER -->\r\n");
      out.write("<link href=\"Component/css/jquery.bxslider.css\" rel=\"stylesheet\">\r\n");
      out.write("<!-- Boostrap Slider -->\r\n");
      out.write("<link href=\"Component/css/bootstrap-slider.css\" rel=\"stylesheet\">\r\n");
      out.write("<!-- Widgets -->\r\n");
      out.write("<link href=\"Component/css/widget.css\" rel=\"stylesheet\">\r\n");
      out.write("<!-- Owl Carusel -->\r\n");
      out.write("<link href=\"Component/css/owl.carousel.css\" rel=\"stylesheet\">\r\n");
      out.write("<!-- responsive -->\r\n");
      out.write("<link href=\"Component/css/responsive.css\" rel=\"stylesheet\">\r\n");
      out.write("<!-- Component -->\r\n");
      out.write("<link href=\"Component/js/dl-menu/component.css\" rel=\"stylesheet\">\r\n");
      out.write("<!-- Font-Awesome -->\r\n");
      out.write("<script src=\"https://kit.fontawesome.com/db62dd06f6.js\"\r\n");
      out.write("	crossorigin=\"anonymous\"></script>\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("	<div id=\"loader-wrapper\">\r\n");
      out.write("		<div id=\"loader\"></div>\r\n");
      out.write("\r\n");
      out.write("		<div class=\"loader-section section-left\"></div>\r\n");
      out.write("		<div class=\"loader-section section-right\"></div>\r\n");
      out.write("\r\n");
      out.write("	</div>\r\n");
      out.write("	<!--WRAPPER START-->\r\n");
      out.write("	<div class=\"wrapper kode-header-class-3\">\r\n");
      out.write("		");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "Header.jsp", out, false);
      out.write("\r\n");
      out.write("		<!--CONTENT START-->\r\n");
      out.write("		<div class=\"kode-content\">\r\n");
      out.write("			<!--AUTHOR DETAIL SECTION START-->\r\n");
      out.write("			<section class=\"kode-author-detail-2\">\r\n");
      out.write("				<div class=\"container\">\r\n");
      out.write("					<div class=\"kode-thumb\">\r\n");
      out.write("						<img src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${authorDetail.authorImg}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("\" alt=\"\">\r\n");
      out.write("					</div>\r\n");
      out.write("					<div class=\"kode-text\">\r\n");
      out.write("						<h2>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${authorDetail.authorName }", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("</h2>\r\n");
      out.write("						<h5>Co-Founder &amp; Author</h5>\r\n");
      out.write("						<div class=\"contact-box\">\r\n");
      out.write("							<div class=\"row\">\r\n");
      out.write("								<div class=\"col-md-8\">\r\n");
      out.write("									<table>\r\n");
      out.write("										<tr>\r\n");
      out.write("											<td><i class=\"fa fa-phone\"></i></td>\r\n");
      out.write("											<td>Phone No:</td>\r\n");
      out.write("											<td>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${authorDetail.phone}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("</td>\r\n");
      out.write("										</tr>\r\n");
      out.write("										<tr>\r\n");
      out.write("											<td><i class=\"fa fa-envelope-o\"></i></td>\r\n");
      out.write("											<td>Email ID:</td>\r\n");
      out.write("											<td>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${authorDetail.email}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("</td>\r\n");
      out.write("										</tr>\r\n");
      out.write("										<tr>\r\n");
      out.write("											<td><i class=\"fa fa-fax\"></i></td>\r\n");
      out.write("											<td>Fax No:</td>\r\n");
      out.write("											<td>333-365-9874</td>\r\n");
      out.write("										</tr>\r\n");
      out.write("									</table>\r\n");
      out.write("								</div>\r\n");
      out.write("								<div class=\"col-md-4\">\r\n");
      out.write("									<div class=\"social-icon\">\r\n");
      out.write("										<ul>\r\n");
      out.write("											<li><a href=\"#\"><i class=\"fa fa-facebook\"></i></a></li>\r\n");
      out.write("											<li><a href=\"#\"><i class=\"fa fa-twitter\"></i></a></li>\r\n");
      out.write("											<li><a href=\"#\"><i class=\"fa fa-pinterest-p\"></i></a></li>\r\n");
      out.write("											<li><a href=\"#\"><i class=\"fa fa-google-plus\"></i></a></li>\r\n");
      out.write("											<li><a href=\"#\"><i class=\"fa fa-linkedin\"></i></a></li>\r\n");
      out.write("											<li><a href=\"#\"><i class=\"fa fa-vimeo\"></i></a></li>\r\n");
      out.write("										</ul>\r\n");
      out.write("									</div>\r\n");
      out.write("								</div>\r\n");
      out.write("							</div>\r\n");
      out.write("						</div>\r\n");
      out.write("						<p>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${authorDetail.authorDesc}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
      out.write("</p>\r\n");
      out.write("						<div class=\"signature\">\r\n");
      out.write("							<img src=\"Component/images/signature-1.png\" alt=\"\">\r\n");
      out.write("						</div>\r\n");
      out.write("					</div>\r\n");
      out.write("				</div>\r\n");
      out.write("			</section>\r\n");
      out.write("			<!--AUTHOR DETAIL SECTION END-->\r\n");
      out.write("			<!--KODE BIOGRAPHY SECTION START-->\r\n");
      out.write("			<section class=\"kode-bio\">\r\n");
      out.write("				<div class=\"container\">\r\n");
      out.write("					<div class=\"section-heading-1\">\r\n");
      out.write("						<h2>Biography</h2>\r\n");
      out.write("						<div class=\"kode-icon\">\r\n");
      out.write("							<i class=\"fa fa-book\"></i>\r\n");
      out.write("						</div>\r\n");
      out.write("					</div>\r\n");
      out.write("					<div class=\"kode-text\">\r\n");
      out.write("						<p>Pages you view in incognito tabs wonât stick around in\r\n");
      out.write("							your browserâs history, cookie store, or search history after\r\n");
      out.write("							youâve closed all of your incognito tabs. Any files you\r\n");
      out.write("							download or bookmarks you create will be kept cookie store, or\r\n");
      out.write("							search history after youâve closed all of your incognito tabs.\r\n");
      out.write("							cookie store, or search history after youâve closed all of your\r\n");
      out.write("							incognito tabs. in your browserâs history, cookie store, or\r\n");
      out.write("							search history after youâve closed all of your incognito tabs</p>\r\n");
      out.write("					</div>\r\n");
      out.write("					<div class=\"kode-text\">\r\n");
      out.write("						<div class=\"row\">\r\n");
      out.write("							<div class=\"col-md-6\">\r\n");
      out.write("								<h2>Early Life and Education</h2>\r\n");
      out.write("								<p>Pages you view in incognito tabs wonât stick around in\r\n");
      out.write("									your browserâs history, cookie store, or search history after\r\n");
      out.write("									youâve closed all of your incognito tabs. Any files you\r\n");
      out.write("									download or bookmarks you create will be kept cookie store, or\r\n");
      out.write("									search history after youâve closed all of your incognito\r\n");
      out.write("									tabs. cookie store, or search history after youâve closed all\r\n");
      out.write("									of your incognito tabs. in your browserâs history, cookie\r\n");
      out.write("									store, or search history after youâve closed all of your\r\n");
      out.write("									incognito tabs</p>\r\n");
      out.write("							</div>\r\n");
      out.write("							<div class=\"col-md-6\">\r\n");
      out.write("								<h2>Early Life and Education</h2>\r\n");
      out.write("								<p>Pages you view in incognito tabs wonât stick around in\r\n");
      out.write("									your browserâs history, cookie store, or search history after\r\n");
      out.write("									youâve closed all of your incognito tabs. Any files you\r\n");
      out.write("									download or bookmarks you create will be kept cookie store, or\r\n");
      out.write("									search history after youâve closed all of your incognito\r\n");
      out.write("									tabs. cookie store, or search history after youâve closed all\r\n");
      out.write("									of your incognito tabs. in your browserâs history, cookie\r\n");
      out.write("									store, or search history after youâve closed all of your\r\n");
      out.write("									incognito tabs</p>\r\n");
      out.write("							</div>\r\n");
      out.write("						</div>\r\n");
      out.write("					</div>\r\n");
      out.write("				</div>\r\n");
      out.write("			</section>\r\n");
      out.write("			<!--KODE BIOGRAPHY SECTION END-->\r\n");
      out.write("			<!--RECENT RELEASE SECTION START-->\r\n");
      out.write("			<section class=\"recent-release\">\r\n");
      out.write("				<div class=\"container\">\r\n");
      out.write("					<div class=\"section-heading-1\">\r\n");
      out.write("						<h2>Published Books</h2>\r\n");
      out.write("						<div class=\"kode-icon\">\r\n");
      out.write("							<i class=\"fa fa-book\"></i>\r\n");
      out.write("						</div>\r\n");
      out.write("					</div>\r\n");
      out.write("					<div class=\"owl-release owl-theme\">\r\n");
      out.write("						");
      if (_jspx_meth_c_005fforEach_005f0(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("					</div>\r\n");
      out.write("				</div>\r\n");
      out.write("			</section>\r\n");
      out.write("			<!--RECENT RELEASE SECTION END-->\r\n");
      out.write("			<!--COUNT UP SECTION START-->\r\n");
      out.write("			<div class=\"lib-count-up-section\">\r\n");
      out.write("				<div class=\"container\">\r\n");
      out.write("					<div class=\"row\">\r\n");
      out.write("						<div class=\"col-md-3 col-sm-6\">\r\n");
      out.write("							<div class=\"count-up\">\r\n");
      out.write("								<span class=\"counter circle\">21</span>\r\n");
      out.write("								<p>Working Year</p>\r\n");
      out.write("							</div>\r\n");
      out.write("						</div>\r\n");
      out.write("						<div class=\"col-md-3 col-sm-6\">\r\n");
      out.write("							<div class=\"count-up\">\r\n");
      out.write("								<span class=\"counter circle\">8589</span>\r\n");
      out.write("								<p>Books Sold</p>\r\n");
      out.write("							</div>\r\n");
      out.write("						</div>\r\n");
      out.write("						<div class=\"col-md-3 col-sm-6\">\r\n");
      out.write("							<div class=\"count-up\">\r\n");
      out.write("								<span class=\"counter circle\">458</span>\r\n");
      out.write("								<p>Top Author</p>\r\n");
      out.write("							</div>\r\n");
      out.write("						</div>\r\n");
      out.write("						<div class=\"col-md-3 col-sm-6\">\r\n");
      out.write("							<div class=\"count-up\">\r\n");
      out.write("								<span class=\"counter circle\">750</span>\r\n");
      out.write("								<p>Book Published</p>\r\n");
      out.write("							</div>\r\n");
      out.write("						</div>\r\n");
      out.write("					</div>\r\n");
      out.write("				</div>\r\n");
      out.write("			</div>\r\n");
      out.write("			<!--COUNT UP SECTION END-->\r\n");
      out.write("\r\n");
      out.write("			<!--GIFT CARD SECTION START-->\r\n");
      out.write("			<section class=\"lib-textimonials\">\r\n");
      out.write("				<div class=\"container\">\r\n");
      out.write("					<!--SECTION HEADING START-->\r\n");
      out.write("					<div class=\"section-heading-1 dark-sec\">\r\n");
      out.write("						<h2>Our Testimonials</h2>\r\n");
      out.write("						<p>What our clients say about the books reviews and comments</p>\r\n");
      out.write("						<div class=\"kode-icon\">\r\n");
      out.write("							<i class=\"fa fa-book\"></i>\r\n");
      out.write("						</div>\r\n");
      out.write("					</div>\r\n");
      out.write("					<!--SECTION HEADING END-->\r\n");
      out.write("					<div class=\"owl-testimonials owl-theme\">\r\n");
      out.write("						<!--BLOG ITEM START-->\r\n");
      out.write("						<div class=\"item\">\r\n");
      out.write("							<div class=\"lib-testimonial-content\">\r\n");
      out.write("								<div class=\"kode-text\">\r\n");
      out.write("									<p>I loved thrift books! It's refreshing to buy discounted\r\n");
      out.write("										books and have them shipped quickly. I could afford to buy 3\r\n");
      out.write("										copies to hand out to friends also interested in the topic.\r\n");
      out.write("										Thank you!! Read more</p>\r\n");
      out.write("								</div>\r\n");
      out.write("								<div class=\"kode-thumb\">\r\n");
      out.write("									<img src=\"Component/images/testimonials1.png\" alt=\"\">\r\n");
      out.write("								</div>\r\n");
      out.write("								<h4>Jenifer Robbert</h4>\r\n");
      out.write("								<p class=\"title\">Author</p>\r\n");
      out.write("							</div>\r\n");
      out.write("						</div>\r\n");
      out.write("						<!--BLOG ITEM END-->\r\n");
      out.write("						<!--BLOG ITEM START-->\r\n");
      out.write("						<div class=\"item\">\r\n");
      out.write("							<div class=\"lib-testimonial-content\">\r\n");
      out.write("								<div class=\"kode-text\">\r\n");
      out.write("									<p>You have great prices and the books are in the shape as\r\n");
      out.write("										stated. Although it takes so long for them to get to their\r\n");
      out.write("										destination. I have been ordering for years and get great\r\n");
      out.write("										books in the shape said.</p>\r\n");
      out.write("								</div>\r\n");
      out.write("								<div class=\"kode-thumb\">\r\n");
      out.write("									<img src=\"Component/images/testimonials-img4.png\" alt=\"\">\r\n");
      out.write("								</div>\r\n");
      out.write("								<h4>Jenifer Robbert</h4>\r\n");
      out.write("								<p class=\"title\">Author</p>\r\n");
      out.write("							</div>\r\n");
      out.write("						</div>\r\n");
      out.write("						<!--BLOG ITEM END-->\r\n");
      out.write("						<!--BLOG ITEM START-->\r\n");
      out.write("						<div class=\"item\">\r\n");
      out.write("							<div class=\"lib-testimonial-content\">\r\n");
      out.write("								<div class=\"kode-text\">\r\n");
      out.write("									<p>I have made many orders with Thrift Books. I always get\r\n");
      out.write("										exactly what I order in a timely manner at a great price. I\r\n");
      out.write("										have had to contact the customer service team once.</p>\r\n");
      out.write("								</div>\r\n");
      out.write("								<div class=\"kode-thumb\">\r\n");
      out.write("									<img src=\"Component/images/testimonials-img3.png\" alt=\"\">\r\n");
      out.write("								</div>\r\n");
      out.write("								<h4>Jenifer Robbert</h4>\r\n");
      out.write("								<p class=\"title\">Author</p>\r\n");
      out.write("							</div>\r\n");
      out.write("						</div>\r\n");
      out.write("						<!--BLOG ITEM END-->\r\n");
      out.write("						<!--BLOG ITEM START-->\r\n");
      out.write("						<div class=\"item\">\r\n");
      out.write("							<div class=\"lib-testimonial-content\">\r\n");
      out.write("								<div class=\"kode-text\">\r\n");
      out.write("									<p>I couldn't believe the prices for such great books, at\r\n");
      out.write("										no shipping! I am going to be a good customer at your store!\r\n");
      out.write("										And, I am telling my Facebook friends about.</p>\r\n");
      out.write("								</div>\r\n");
      out.write("								<div class=\"kode-thumb\">\r\n");
      out.write("									<img src=\"Component/images/testimonials-img2.png\" alt=\"\">\r\n");
      out.write("								</div>\r\n");
      out.write("								<h4>Jenifer Robbert</h4>\r\n");
      out.write("								<p class=\"title\">Author</p>\r\n");
      out.write("							</div>\r\n");
      out.write("						</div>\r\n");
      out.write("						<!--BLOG ITEM END-->\r\n");
      out.write("						<!--BLOG ITEM START-->\r\n");
      out.write("						<div class=\"item\">\r\n");
      out.write("							<div class=\"lib-testimonial-content\">\r\n");
      out.write("								<div class=\"kode-text\">\r\n");
      out.write("									<p>ordered 14 books, received 14 books within a week. very\r\n");
      out.write("										happy with customer support and with the receipt of books.\r\n");
      out.write("										Keep It Up Good Guide we love you the best books library\r\n");
      out.write("										available today.</p>\r\n");
      out.write("								</div>\r\n");
      out.write("								<div class=\"kode-thumb\">\r\n");
      out.write("									<img src=\"Component/images/writer2.png\" alt=\"\">\r\n");
      out.write("								</div>\r\n");
      out.write("								<h4>Jenifer Robbert</h4>\r\n");
      out.write("								<p class=\"title\">Author</p>\r\n");
      out.write("							</div>\r\n");
      out.write("						</div>\r\n");
      out.write("						<!--BLOG ITEM END-->\r\n");
      out.write("						<!--BLOG ITEM START-->\r\n");
      out.write("						<div class=\"item\">\r\n");
      out.write("							<div class=\"lib-testimonial-content\">\r\n");
      out.write("								<div class=\"kode-text\">\r\n");
      out.write("									<p>Thrift Books is the absolute best book seller on the\r\n");
      out.write("										Internet!! Their selection is marvelous, price/shipping\r\n");
      out.write("										unbeatable and timely service is outstanding.</p>\r\n");
      out.write("								</div>\r\n");
      out.write("								<div class=\"kode-thumb\">\r\n");
      out.write("									<img src=\"Component/images/writer3.png\" alt=\"\">\r\n");
      out.write("								</div>\r\n");
      out.write("								<h4>Jenifer Robbert</h4>\r\n");
      out.write("								<p class=\"title\">Author</p>\r\n");
      out.write("							</div>\r\n");
      out.write("						</div>\r\n");
      out.write("						<!--BLOG ITEM END-->\r\n");
      out.write("					</div>\r\n");
      out.write("				</div>\r\n");
      out.write("			</section>\r\n");
      out.write("			<!--GIFT CARD SECTION END-->\r\n");
      out.write("		</div>\r\n");
      out.write("		");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "Footer.jsp", out, false);
      out.write("\r\n");
      out.write("	</div>\r\n");
      out.write("	<!--WRAPPER END-->\r\n");
      out.write("	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->\r\n");
      out.write("	<script src=\"Component/js/jquery.min.js\"></script>\r\n");
      out.write("	<!-- Include all compiled plugins (below), or include individual files as needed -->\r\n");
      out.write("	<script src=\"Component/js/bootstrap.min.js\"></script>\r\n");
      out.write("	<script src=\"Component/js/dl-menu/modernizr.custom.js\"></script>\r\n");
      out.write("	<script src=\"Component/js/dl-menu/jquery.dlmenu.js\"></script>\r\n");
      out.write("	<script src=\"Component/js/jquery.bxslider.min.js\"></script>\r\n");
      out.write("	<script src=\"Component/js/bootstrap-slider.js\"></script>\r\n");
      out.write("	<script src=\"Component/js/waypoints.min.js\"></script>\r\n");
      out.write("	<script src=\"Component/js/jquery.counterup.min.js\"></script>\r\n");
      out.write("	<script src=\"Component/js/owl.carousel.js\"></script>\r\n");
      out.write("	<script src=\"Component/js/functions.js\"></script>\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try {
            if (response.isCommitted()) {
              out.flush();
            } else {
              out.clearBuffer();
            }
          } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_005fforEach_005f0(javax.servlet.jsp.PageContext _jspx_page_context)
          throws java.lang.Throwable {
    javax.servlet.jsp.PageContext pageContext = _jspx_page_context;
    javax.servlet.jsp.JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_005fforEach_005f0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _005fjspx_005ftagPool_005fc_005fforEach_0026_005fvar_005fitems.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    boolean _jspx_th_c_005fforEach_005f0_reused = false;
    try {
      _jspx_th_c_005fforEach_005f0.setPageContext(_jspx_page_context);
      _jspx_th_c_005fforEach_005f0.setParent(null);
      // /View/AuthorDetail.jsp(163,6) name = items type = javax.el.ValueExpression reqTime = true required = false fragment = false deferredValue = true expectedTypeName = java.lang.Object deferredMethod = false methodSignature = null
      _jspx_th_c_005fforEach_005f0.setItems(new org.apache.jasper.el.JspValueExpression("/View/AuthorDetail.jsp(163,6) '${listBookOfAuthor}'",_jsp_getExpressionFactory().createValueExpression(_jspx_page_context.getELContext(),"${listBookOfAuthor}",java.lang.Object.class)).getValue(_jspx_page_context.getELContext()));
      // /View/AuthorDetail.jsp(163,6) name = var type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
      _jspx_th_c_005fforEach_005f0.setVar("o");
      int[] _jspx_push_body_count_c_005fforEach_005f0 = new int[] { 0 };
      try {
        int _jspx_eval_c_005fforEach_005f0 = _jspx_th_c_005fforEach_005f0.doStartTag();
        if (_jspx_eval_c_005fforEach_005f0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
          do {
            out.write("\r\n");
            out.write("							<div class=\"item\">\r\n");
            out.write("								<div class=\"book-released\">\r\n");
            out.write("									<div class=\"kode-thumb\">\r\n");
            out.write("										<a href=\"#\"><img src=\"");
            out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${o.bookImg }", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
            out.write("\" alt=\"\"></a>\r\n");
            out.write("										<div class=\"cart-btns\">\r\n");
            out.write("											<a href=\"#\" data-toggle=\"tooltip\" title=\"");
            out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${o.viewed }", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
            out.write("\"><i\r\n");
            out.write("												class=\"fa fa-eye\"></i></a> <a href=\"#\" data-toggle=\"tooltip\"\r\n");
            out.write("												title=\"Add To Cart\"><i class=\"fa fa-shopping-cart\"></i></a>\r\n");
            out.write("											<a href=\"#\" data-toggle=\"tooltip\" title=\"Add To Wishlist\"><i\r\n");
            out.write("												class=\"fa fa-heart-o\"></i></a>\r\n");
            out.write("										</div>\r\n");
            out.write("									</div>\r\n");
            out.write("									<div class=\"kode-text\">\r\n");
            out.write("										<h3>");
            out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${o.bookTitle}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null));
            out.write("</h3>\r\n");
            out.write("\r\n");
            out.write("									</div>\r\n");
            out.write("								</div>\r\n");
            out.write("							</div>\r\n");
            out.write("						");
            int evalDoAfterBody = _jspx_th_c_005fforEach_005f0.doAfterBody();
            if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
              break;
          } while (true);
        }
        if (_jspx_th_c_005fforEach_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
          return true;
        }
      } catch (java.lang.Throwable _jspx_exception) {
        while (_jspx_push_body_count_c_005fforEach_005f0[0]-- > 0)
          out = _jspx_page_context.popBody();
        _jspx_th_c_005fforEach_005f0.doCatch(_jspx_exception);
      } finally {
        _jspx_th_c_005fforEach_005f0.doFinally();
      }
      _005fjspx_005ftagPool_005fc_005fforEach_0026_005fvar_005fitems.reuse(_jspx_th_c_005fforEach_005f0);
      _jspx_th_c_005fforEach_005f0_reused = true;
    } finally {
      org.apache.jasper.runtime.JspRuntimeLibrary.releaseTag(_jspx_th_c_005fforEach_005f0, _jsp_getInstanceManager(), _jspx_th_c_005fforEach_005f0_reused);
    }
    return false;
  }
}
