<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Book Library - Book Guide Author, Publication and Store</title>
<!-- CUSTOM STYLE -->
<link href="Component/css/style.css" rel="stylesheet">
<!-- THEME TYPO -->
<link href="Component/css/themetypo.css" rel="stylesheet">
<!-- BOOTSTRAP -->
<link href="Component/css/bootstrap.css" rel="stylesheet">
<!-- COLOR FILE -->
<link href="Component/css/color.css" rel="stylesheet">
<!-- FONT AWESOME -->
<link href="Component/css/font-awesome.min.css" rel="stylesheet">
<!-- BX SLIDER -->
<link href="Component/css/jquery.bxslider.css" rel="stylesheet">
<!-- Boostrap Slider -->
<link href="Component/css/bootstrap-slider.css" rel="stylesheet">
<!-- Widgets -->
<link href="Component/css/widget.css" rel="stylesheet">
<!-- Owl Carusel -->
<link href="Component/css/owl.carousel.css" rel="stylesheet">
<!-- responsive -->
<link href="Component/css/responsive.css" rel="stylesheet">
<!-- Component -->
<link href="Component/js/dl-menu/component.css" rel="stylesheet">
<!-- Font-Awesome -->
<script src="https://kit.fontawesome.com/db62dd06f6.js" crossorigin="anonymous"></script>

</head>
<body>
<div id="loader-wrapper">
	<div id="loader"></div>

	<div class="loader-section section-left"></div>
	<div class="loader-section section-right"></div>

</div>
<!--WRAPPER START-->
<div class="wrapper kode-header-class-3">
	<jsp:include page="Header.jsp"></jsp:include>
    <!--CONTENT START-->
    <div class="kode-content padding-tb-50">
    	<div class="container">
            <div class="row">
                
                <div class="col-md-12">
                    <!--BOOK DETAIL START-->
                    <div class="lib-book-detail">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="kode-thumb">
                                    <img src="${bookDetail.bookImg}" alt="">
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="kode-text">
                                	<h2>${bookDetail.bookTitle}</h2>
                                    <div class="book-text">
                                    	<p>${bookDetail.brief}</p>
                                    </div>
                                    <div class="book-text">
                                    	<p>Category: Books.</p>
                                        <p>Tag: books.</p>
                                        <p>Author: Daniel Abraham</p>
                                        <p>Publisher: ${bookDetail.publisher}</p>
                                        <p>Product ID: ${bookDetail.bookId}</p>
                                    </div>
                                    <a href="addBookCase?bookId=${bookDetail.bookId}" class="add-to-cart">Add To Favorite</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--BOOK DETAIL END-->
                    <!--PRODUCT REVIEW TABS START-->
                    <div class="product-review-tabs">
                <!--NAV TABS START-->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#Description " aria-controls="Description" role="tab" data-toggle="tab">Description </a></li>
                </ul>
                <!--NAV TABS END-->
                <!--TAB PANEL START-->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="Description">
                        <p>${bookDetail.content}</p>
                    </div>
                </div>
                <!--TAB PANEL END-->
            </div>
                    <!--PRODUCT REVIEW TABS END-->
                    <!--RELATED PRODUCTS START-->
                    <div class="lib-related-products">
                        <h2>Related Books</h2>
                        <div class="row">
                        <c:forEach items="${listBook}" var="b">
                            <!--PRODUCT GRID START-->
                            <div class="col-md-4 col-sm-6">
                                <div class="best-seller-pro">
                                     <figure>
                            	<img src="${b.bookImg}" alt="">
                            </figure>
                            <div class="kode-text">
                            	<h3><a href="">${b.bookTitle}</a></h3>
                            </div>
                            <div class="kode-caption">
                            	<h3>${b.bookTitle}</h3>
                                
                                <a href="detail?id=${b.bookId}" class="add-to-cart"><i class="fa-solid fa-heart"></i> View</a>
                                    </div>
                                </div>
                            </div>
                            <!--PRODUCT GRID END-->
                          </c:forEach>
                        </div>
                    </div>
                    <!--RELATED PRODUCTS END-->
                </div>
            </div>
        </div>
        </div> 
	<jsp:include page="Footer.jsp"></jsp:include>
</div>
<!--WRAPPER END-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="Component/js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="Component/js/bootstrap.min.js"></script>
<script src="Component/js/jquery.bxslider.min.js"></script>
<script src="Component/js/bootstrap-slider.js"></script>
<script src="Component/js/waypoints.min.js"></script> 
<script src="Component/js/jquery.counterup.min.js"></script>
<script src="Component/js/owl.carousel.js"></script>
<script src="Component/js/dl-menu/modernizr.custom.js"></script>
<script src="Component/js/dl-menu/jquery.dlmenu.js"></script>
<script src="Component/js/functions.js"></script>
</body>
</html>