<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Book Library - Book Guide Author, Publication and Store</title>
<!-- CUSTOM STYLE -->
<link href="Component/css/style.css" rel="stylesheet">
<!-- THEME TYPO -->
<link href="Component/css/themetypo.css" rel="stylesheet">
<!-- SHORTCODES -->
<link href="Component/css/shortcode.css" rel="stylesheet">
<!-- BOOTSTRAP -->
<link href="Component/css/bootstrap.css" rel="stylesheet">
<!-- COLOR FILE -->
<link href="Component/css/color.css" rel="stylesheet">
<!-- FONT AWESOME -->
<link href="Component/css/font-awesome.min.css" rel="stylesheet">
<!-- BX SLIDER -->
<link href="Component/css/jquery.bxslider.css" rel="stylesheet">
<!-- Boostrap Slider -->
<link href="Component/css/bootstrap-slider.css" rel="stylesheet">
<!-- Widgets -->
<link href="Component/css/widget.css" rel="stylesheet">
<!-- Owl Carusel -->
<link href="Component/css/owl.carousel.css" rel="stylesheet">
<!-- responsive -->
<link href="Component/css/responsive.css" rel="stylesheet">
<!-- Component -->
<link href="Component/js/dl-menu/component.css" rel="stylesheet">
<!-- Font-Awesome -->
<script src="https://kit.fontawesome.com/db62dd06f6.js" crossorigin="anonymous"></script>
</head>
<body>
<div id="loader-wrapper">
	<div id="loader"></div>

	<div class="loader-section section-left"></div>
	<div class="loader-section section-right"></div>

</div>
<!--WRAPPER START-->
<div class="wrapper kode-header-class-3">
	<jsp:include page="Header.jsp"></jsp:include>
    
    <!--CONTENT START-->
    <div class="kode-content">
        <!--BOOK GUIDE SECTION START-->
        <section class="kode-about-us-section">
        	<div class="container">
            	<!--SECTION CONTENT START-->
            	<div class="heading-1">
                	<h2>Give That You Like Never Before</h2>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem</p>
                </div>
                <!--SECTION CONTENT END-->
                <div class="row">
                	<div class="col-md-6">
                    	<h2>Who we are?</h2>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
                        <ul class="kd-list3">
                            <li><a href="#">Consetetur sadipscing elitr sed diam nonumy eirmod</a></li>
                            <li><a href="#">Sadipscing elitr sed diam nonumy eirmod</a></li>
                            <li><a href="#">Consetetur elitr sed diam nonumy eirmod</a></li>
                            <li><a href="#">Nonumy elitr sed diam nonumy eirmod</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                    	<div class="about-thumb">
                        	<img src="Component/images/about-img3.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--BOOK GUIDE SECTION END-->
        <!--BOOKS SECTION START-->
        <section class="about-lib-theme">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2>Meet Our Team</h2>
                        <div class="row">
                        	<div class="col-md-6 col-sm-6">
                            	<div class="kode-team-listing">
                                	<div class="kode-thumb">
                                    	<a href="#"><img src="Component/images/team-1.png" alt=""></a>
                                    </div>
                                    <div class="kode-text">
                                    	<h3>Jenny</h3>
                                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</p>
                                        <div class="social-icon">
                                        	<ul>
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>                      
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                            	<div class="kode-team-listing">
                                	<div class="kode-thumb">
                                    	<a href="#"><img src="Component/images/team-2.png" alt=""></a>
                                    </div>
                                    <div class="kode-text">
                                    	<h3>Rose</h3>
                                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</p>
                                        <div class="social-icon">
                                        	<ul>
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>                      
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                            	<div class="kode-team-listing">
                                	<div class="kode-thumb">
                                    	<a href="#"><img src="Component/images/team-3.png" alt=""></a>
                                    </div>
                                    <div class="kode-text">
                                    	<h3>John Doe</h3>
                                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</p>
                                        <div class="social-icon">
                                        	<ul>
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>                      
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                            	<div class="kode-team-listing">
                                	<div class="kode-thumb">
                                    	<a href="#"><img src="Component/images/team-4.png" alt=""></a>
                                    </div>
                                    <div class="kode-text">
                                    	<h3>Nina Williams</h3>
                                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</p>
                                        <div class="social-icon">
                                        	<ul>
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>                      
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                    	<h2>Our Dealing in projects</h2>
                        <div class="lib-qualities">
                             <ul>   
                                <!--LIST ITEM START-->
                                <li>
                                    <i class="fa fa-star"></i>
                                    <h2>100% Client's satisfaction</h2>
                                    <p>We provide 100% customer professional support and clean code to satisfy our clients.</p>
                                </li>
                                <!--LIST ITEM END-->
                                <!--LIST ITEM START-->
                                <li>
                                    <i class="fa fa-leaf"></i>
                                    <h2>User friendly Design Quality</h2>
                                    <p>The desgin of the book library is very user friendly easy to use and customize the design and site.</p>
                                </li>
                                <!--LIST ITEM END-->
                                <!--LIST ITEM START-->
                                <li>
                                    <i class="fa fa-users"></i>
                                    <h2>Great Impression on website visitors</h2>
                                    <p>The exciting look of the book library gives a wonder full impression to the visitors and provide the traffic.</p>
                                </li>
                                <!--LIST ITEM END-->
                                <!--LIST ITEM START-->
                                <li>
                                    <i class="fa fa-life-ring"></i>
                                    <h2>24 hours quick support</h2>
                                    <p>We provide 24 / 7 custom support to our customer because we never leave our customers alone.</p>
                                </li>
                                <!--LIST ITEM END-->
                            </ul>
                        </div>                        
                    </div>                    
                </div>                
            </div>
        </section>
        <!--BOOKS SECTION END-->
        <!--LIBRARY GALLERY SECTION START-->
        <section class="our-libraries">
        	<div class="container">
            	<h2>Our Libraries</h2>
                <div class="our-libraries-cover">
                	<div class="owl-library">
                    <div class="item">
                    	<div class="kode-thumb">
                        	<a href="#"><img src="Component/images/library-1.png" alt="Library"></a>
                    	</div>
                    </div>
                    <div class="item">
                    	<div class="kode-thumb">
                        	<a href="#"><img src="Component/images/library-2.png" alt="Library"></a>
                    	</div>
                    </div>
                    <div class="item">
                    	<div class="kode-thumb">
                        	<a href="#"><img src="Component/images/library-3.png" alt="Library"></a>
                    	</div>
                    </div>
                    <div class="item">
                    	<div class="kode-thumb">
                        	<a href="#"><img src="Component/images/library-4.png" alt="Library"></a>
                    	</div>
                    </div>
                    <div class="item">
                    	<div class="kode-thumb">
                        	<a href="#"><img src="Component/images/library-1.png" alt="Library"></a>
                    	</div>
                    </div>
                    <div class="item">
                    	<div class="kode-thumb">
                        	<a href="#"><img src="Component/images/library-2.png" alt="Library"></a>
                    	</div>
                    </div>
                    <div class="item">
                    	<div class="kode-thumb">
                        	<a href="#"><img src="Component/Component/images/library-3.png" alt="Library"></a>
                    	</div>
                    </div>
                    <div class="item">
                    	<div class="kode-thumb">
                        	<a href="#"><img src="Component/images/library-4.png" alt="Library"></a>
                    	</div>
                    </div>
                    
                </div>
                </div>
            </div>
        </section>
        <!--LIBRARY GALLERY SECTION END-->
        <!--FACTS SECTION START-->
        <section class="kode-interesting-facts">
            <div class="container">
				<h2>Interesting Facts</h2>
                <div class="kode-facts">
                	<!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#work" aria-controls="work" role="tab" data-toggle="tab">Why work with us</a></li>
                        <li role="presentation"><a href="#Benifits" aria-controls="Benifits" role="tab" data-toggle="tab">Benifits &amp; perks</a></li>
                        <li role="presentation"><a href="#located" aria-controls="located" role="tab" data-toggle="tab">Where we located</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="work">
                        	<h4>Why Work with us</h4>
                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="Benifits">
                        	<h4>Benifits &amp; Perks</h4>
                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
                            <p> Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="located">
                        	<h4>Where we located?</h4>
                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
	</div>	
	<jsp:include page="Footer.jsp"></jsp:include>
</div>
<!--WRAPPER END-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="Component/js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="Component/js/bootstrap.min.js"></script>
<script src="Component/js/dl-menu/modernizr.custom.js"></script>
<script src="Component/js/dl-menu/jquery.dlmenu.js"></script>
<script src="Component/js/jquery.bxslider.min.js"></script>
<script src="Component/js/bootstrap-slider.js"></script>
<script src="Component/js/waypoints.min.js"></script> 
<script src="Component/js/jquery.counterup.min.js"></script>
<script src="Component/js/owl.carousel.js"></script>
<script src="Component/js/functions.js"></script>
</body>
</html>