<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Book Library - Book Guide Author, Publication and Store</title>
<!-- CUSTOM STYLE -->
<link href="Component/css/style.css" rel="stylesheet">
<!-- THEME TYPO -->
<link href="Component/css/themetypo.css" rel="stylesheet">
<!-- BOOTSTRAP -->
<link href="Component/css/bootstrap.css" rel="stylesheet">
<!-- COLOR FILE -->
<link href="Component/css/color.css" rel="stylesheet">
<!-- FONT AWESOME -->
<link href="Component/css/font-awesome.min.css" rel="stylesheet">
<!-- BX SLIDER -->
<link href="Component/css/jquery.bxslider.css" rel="stylesheet">
<!-- Boostrap Slider -->
<link href="Component/css/bootstrap-slider.css" rel="stylesheet">
<!-- Widgets -->
<link href="Component/css/widget.css" rel="stylesheet">
<!-- Owl Carusel -->
<link href="Component/css/owl.carousel.css" rel="stylesheet">
<!-- responsive -->
<link href="Component/css/responsive.css" rel="stylesheet">
<!-- Component -->
<link href="Component/js/dl-menu/component.css" rel="stylesheet">
<!-- Font-Awesome -->
<script src="https://kit.fontawesome.com/db62dd06f6.js"
	crossorigin="anonymous"></script>

</head>
<body>
	<div id="loader-wrapper">
		<div id="loader"></div>

		<div class="loader-section section-left"></div>
		<div class="loader-section section-right"></div>

	</div>
	<!--WRAPPER START-->
	<div class="wrapper kode-header-class-3">
		<jsp:include page="Header.jsp"></jsp:include>
		<!--CONTENT START-->
		<div class="kode-content padding-tb-50">
			<div class="container">

				<div class="row">
					<div class="col-md-8">
						<div class="comment-form">
							<div class="row">
								<form action="contactUs" method="POST">
									<div class="col-md-4 col-sm-4">
										<div class="input-container">
											<input type="text" id="name" name="name" class="required"
												placeholder="Name *" /> <label for="name">Name</label>
										</div>
									</div>
									<div class="col-md-4 col-sm-4">
										<div class="input-container">
											<input type="text" id="email" name="email"
												class="required email" placeholder="Email *"> <label
												for="email">Email</label>
										</div>
									</div>
									<div class="col-md-4 col-sm-4">
										<div class="input-container">
											<input type="text" id="phone" name="phone" class="required"
												placeholder="Phone *"> <label for="phone">Phone</label>
										</div>
									</div>
									<div class="col-md-12 col-sm-12">
										<div class="input-container">
											<textarea name="message" id="message"
												placeholder="add your comment"></textarea>
											<label for="message">Message</label>
										</div>
									</div>
									<div class="col-md-6">
										<c:if test="${alert != null}">
											<h4 style="color: green;">
												<c:out value="${alert}" />
											</h4>
										</c:if>
									</div>
									<div class="col-md-6">
										<p class="kd-button kf_submit widget-newslatter pull-right">
											<input class="thbg-color" type="submit" value="Submit">
										</p>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="col-md-4 sidebar">
						<div class="widget widget-text">
							<h2>get in touch</h2>
							<p>Duis aute irure dolor in reprehenderit in voluptate velit
								esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
								occaecat cupidatat non proident</p>
						</div>
						<div class="widget widget-text">
							<h2>Information</h2>
							<ul>
								<li><i class="fa fa-map-marker"></i>Nemo enim ipsam
									voluptatem quia voluptas sit</li>
								<li><i class="fa fa-phone"></i>(25) 82 800 80</li>
								<li><i class="fa fa-envelope"></i><a
									href="mailto:info@librarytheme.com">info@gmail.com</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<jsp:include page="Footer.jsp"></jsp:include>
	</div>
	<!--WRAPPER END-->
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="Component/js/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="Component/js/bootstrap.min.js"></script>
	<script src="Component/js/jquery.bxslider.min.js"></script>
	<script src="Component/js/bootstrap-slider.js"></script>
	<script src="Component/js/waypoints.min.js"></script>
	<script src="Component/js/jquery.counterup.min.js"></script>
	<script src="Component/js/owl.carousel.js"></script>
	<script
		src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
	<script src="Component/js/dl-menu/modernizr.custom.js"></script>
	<script src="Component/js/dl-menu/jquery.dlmenu.js"></script>
	<script src="Component/js/functions.js"></script>
	
</body>
</html>