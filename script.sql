USE [BMS]
GO
/****** Object:  Table [dbo].[Author]    Script Date: 8/16/2022 10:07:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Author](
	[author_id] [int] IDENTITY(1,1) NOT NULL,
	[author_name] [varchar](50) NULL,
	[author_Desc] [varchar](max) NULL,
	[phone] [varchar](13) NULL,
	[email] [varchar](100) NULL,
	[author_img] [varchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[author_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Book]    Script Date: 8/16/2022 10:07:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Book](
	[book_id] [varchar](6) NOT NULL,
	[book_title] [varchar](50) NULL,
	[brief] [varchar](500) NULL,
	[publisher] [varchar](50) NULL,
	[content] [varchar](max) NULL,
	[book_img] [varchar](250) NULL,
	[viewed] [int] NULL,
	[Release_Date] [date] NULL,
	[category] [int] NULL,
	[author_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[book_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BookCase]    Script Date: 8/16/2022 10:07:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BookCase](
	[book_case_id] [int] NOT NULL,
	[book_case_name] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[book_case_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 8/16/2022 10:07:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[category_id] [int] IDENTITY(1,1) NOT NULL,
	[category_name] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[category_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContactUs]    Script Date: 8/16/2022 10:07:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContactUs](
	[contact_id] [int] IDENTITY(1,1) NOT NULL,
	[contact_name] [varchar](50) NULL,
	[contact_email] [varchar](100) NULL,
	[contact_phone] [varchar](13) NULL,
	[contact_mess] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[contact_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Contain]    Script Date: 8/16/2022 10:07:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contain](
	[book_case_id] [int] NOT NULL,
	[book_id] [varchar](6) NOT NULL,
	[create_date] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[book_case_id] ASC,
	[book_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 8/16/2022 10:07:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[role_id] [int] NOT NULL,
	[authority] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[role_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 8/16/2022 10:07:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_name] [varchar](50) NULL,
	[password] [varchar](50) NULL,
	[email] [varchar](100) NULL,
	[hash] [varchar](255) NULL,
	[active] [int] NULL,
	[phone] [varchar](50) NULL,
	[dob] [date] NULL,
	[gender] [bit] NULL,
	[image] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Author] ON 

INSERT [dbo].[Author] ([author_id], [author_name], [author_Desc], [phone], [email], [author_img]) VALUES (3, N'Chinua Achebe', N'Chinua Achebe is the #1 New York Times and International bestselling author of multiple novels and novellas. She lives in Texas with her husband and their three boys. She is the founder of The Bookworm Box, a non-profit book subscription service and bookstore in Sulphur Springs, Texas.', N'0983243986', N'abc@gmail.com', N'Component/images/author13.png')
INSERT [dbo].[Author] ([author_id], [author_name], [author_Desc], [phone], [email], [author_img]) VALUES (4, N'Julian Baggini', N'Julian Baggini is a philosopher, journalist and the author of over 20 books about philosophy written for a general audience.', N'0968654732', N'abcd@gmail.com', N'Component/images/author9.png')
INSERT [dbo].[Author] ([author_id], [author_name], [author_Desc], [phone], [email], [author_img]) VALUES (5, N'Quentin Blake', N'Quentin Blake (born 16 December 1932) is an English cartoonist, caricaturist, illustrator and children''s writer. He has illustrated over 300 books, including 18 written by Roald Dahl, which are among his most popular works.', N'0968546575', N'c@gmail.com', N'Component/images/author4.png')
INSERT [dbo].[Author] ([author_id], [author_name], [author_Desc], [phone], [email], [author_img]) VALUES (6, N'Saul Bellow', N'Saul Bellow (born Solomon Bellows; 10 June 1915 – 5 April 2005) was a Canadian-born American writer. For his literary work, Bellow was awarded the Pulitzer Prize, the Nobel Prize for Literature, and the National Medal of Arts', N'0968546575', N'auc@gmail.com', N'Component/images/author5.png')
INSERT [dbo].[Author] ([author_id], [author_name], [author_Desc], [phone], [email], [author_img]) VALUES (7, N'Chinua Achebe', N'Chinua Achebe is the #1 New York Times and International bestselling author of multiple novels and novellas. She lives in Texas with her husband and their three boys. She is the founder of The Bookworm Box, a non-profit book subscription service and bookstore in Sulphur Springs, Texas.', N'0983243986', N'abc@gmail.com', N'Component/images/author6.png')
INSERT [dbo].[Author] ([author_id], [author_name], [author_Desc], [phone], [email], [author_img]) VALUES (8, N'Julian Baggini', N'Julian Baggini is a philosopher, journalist and the author of over 20 books about philosophy written for a general audience.', N'0968654732', N'abcd@gmail.com', N'Component/images/author9.png')
INSERT [dbo].[Author] ([author_id], [author_name], [author_Desc], [phone], [email], [author_img]) VALUES (9, N'Quentin Blake', N'Quentin Blake (born 16 December 1932) is an English cartoonist, caricaturist, illustrator and children''s writer. He has illustrated over 300 books, including 18 written by Roald Dahl, which are among his most popular works.', N'0968546575', N'c@gmail.com', N'Component/images/author7.png')
INSERT [dbo].[Author] ([author_id], [author_name], [author_Desc], [phone], [email], [author_img]) VALUES (10, N'Saul Bellow', N'Saul Bellow (born Solomon Bellows; 10 June 1915 – 5 April 2005) was a Canadian-born American writer. For his literary work, Bellow was awarded the Pulitzer Prize, the Nobel Prize for Literature, and the National Medal of Arts', N'0968546575', N'auc@gmail.com', N'Component/images/author-detail.jpg')
INSERT [dbo].[Author] ([author_id], [author_name], [author_Desc], [phone], [email], [author_img]) VALUES (11, N'Paul Jack', N'Paul Jack has 5 books on Goodreads with 55 ratings. Jack W. Paul''s most popular book is Peach Blossom Cologne Company with CD [With CDROM].', N'0962846575', N'dom@gmail.com', N'Component/images/author9.png')
INSERT [dbo].[Author] ([author_id], [author_name], [author_Desc], [phone], [email], [author_img]) VALUES (12, N'Nina Bawden', N'Nina Bawden CBE, FRSL, JP (19 January 1925 – 22 August 2012) was an English novelist and children''s writer. She was shortlisted for the Booker Prize in 1987 and the Lost Man Booker Prize in 2010. She is one of very few who have both served as a Booker judge and made a Booker shortlist as an author.[1] She was a recipient of the Golden PEN Award.', N'0996546575', N'WAN@gmail.com', N'Component/images/author10.png')
INSERT [dbo].[Author] ([author_id], [author_name], [author_Desc], [phone], [email], [author_img]) VALUES (13, N'Mary Beard', N'Dame Winifred Mary Beard, DBE, FSA, FBA, FRSL (born 1 January 1955)[1] is an English scholar of Ancient Rome.
She is Professor of Classics at the University of Cambridge,[2] a fellow of Newnham College, and Royal Academy of Arts Professor of Ancient Literature. She is the classics editor of The Times Literary Supplement, where she also writes a regular blog, "A Don''s Life"', N'0962346575', N'facet@gmail.com', N'Component/images/author11.png')
INSERT [dbo].[Author] ([author_id], [author_name], [author_Desc], [phone], [email], [author_img]) VALUES (14, N'Belinda Bauer', N'Belinda Bauer (born 1962) is a British writer of crime novels. She grew up in England and South Africa, but later moved to Wales, where she worked as a court reporter in Cardiff; the country is often used as a setting in her work.', N'0968542375', N'somet@gmail.com', N'Component/images/author12.png')
INSERT [dbo].[Author] ([author_id], [author_name], [author_Desc], [phone], [email], [author_img]) VALUES (15, N'Sebastian Barry', N'Sebastian Barry (born 5 July 1955) is an Irish novelist, playwright and poet. He was named Laureate for Irish Fiction, 2019–2021. He is noted for his lyrical literary writing style and is considered one of Ireland''s finest writers.', N'0967658575', N'air@gmail.com', N'Component/images/author13.png')
SET IDENTITY_INSERT [dbo].[Author] OFF
GO
INSERT [dbo].[Book] ([book_id], [book_title], [brief], [publisher], [content], [book_img], [viewed], [Release_Date], [category], [author_id]) VALUES (N'B101', N'Micheal Treacy', N'Sed ut perspiciatis', N'Penguin Publishing Group', N'"A true classic of world literature . . . A masterpiece that has inspired generations of writers in Nigeria, across Africa, and around the world." -- Barack Obama "African literature is incomplete and unthinkable without the works of Chinua Achebe." --Toni Morrison Nominated as one of America''s best-loved novels by PBS''s The Great American Read Things Fall Apart is the first of three novels in Chinua Achebe''s critically acclaimed African Trilogy. It is a classic narrative about Africa''s cataclysmic encounter with Europe as it establishes a colonial presence on the continent. Told through the fictional experiences of Okonkwo, a wealthy and fearless Igbo warrior of Umuofia in the late 1800s, Things Fall Apart explores one man''s futile resistance to the devaluing of his Igbo traditions by British political andreligious forces and his despair as his community capitulates to the powerful new order. With more than 20 million copies sold and translated into fifty-seven languages, Things Fall Apart provides one of the most illuminating and permanent monuments to African experience. Achebe does not only capture life in a pre-colonial African village, he conveys the tragedy of the loss of that world while broadening our understanding of our contemporary realities.', N'Component/images/book.png', 100, CAST(N'2022-08-01' AS Date), 1, 4)
INSERT [dbo].[Book] ([book_id], [book_title], [brief], [publisher], [content], [book_img], [viewed], [Release_Date], [category], [author_id]) VALUES (N'B102', N'Life is a Trip', N'Sed ut perspiciatis', N'Penguin Publishing Group', N'A classic story of moral struggle in an age of turbulent social change and the final book in Chinua Achebe''s The African Trilogy When Obi Okonkwo, grandson of Okonkwo, the main character in Things Fall Apart returns to Nigeria from England in the 1950s, his foreign education separates him from his African roots. No Longer at Ease, the third and concluding novel in Chinua Achebe''s The African Trilogy , depicts the uncertainties that beset the nation of Nigeria, as independence from colonial rule loomed near. In Obi Okonkwo''s experiences, the ambiguities, pitfalls, and temptations of a rapidly evolving society are revealed. He is part of a ruling Nigerian elite whose corruption he finds repugnant. His fate, however, overtakes him as he finds himself trapped between the expectation of his family, his village--both representations of the traditional world of his ancestors--and the colonial world. A story of a man lost in cultural limbo, and a nation entering a new age of disillusionment, No Longer at Ease is a powerful metaphor for his generation of young Nigerians.', N'Component/images/book2.png', 245, CAST(N'2022-08-01' AS Date), 2, 4)
INSERT [dbo].[Book] ([book_id], [book_title], [brief], [publisher], [content], [book_img], [viewed], [Release_Date], [category], [author_id]) VALUES (N'B103', N'Jennifer Veiner', N'Sed ut perspiciatis', N'Penguin Publishing Group', N'The second novel in Chinua Achebe''s masterful African trilogy, following Things Fall Apart and preceding No Longer at Ease When Things Fall Apart ends, colonial rule has been introduced to Umuofia, and the character of the nation, its values, freedoms, religious and socio-political foundations have substantially and irrevocably been altered. Arrow of God, the second novel in Chinua Achebe''s The African Trilogy , moves the historical narrative forward. This time, the action revolves around Ezeulu, the headstrong chief priest of the god Ulu, which is worshipped by the six villages of Umuaro. The novel is a meditation on the nature, uses, and responsibility of power and leadership. Ezeulu finds that his authority is increasingly under threat from rivals within his nation and functionaries of the newly established British colonial government. Yet he sees himself as untouchable. He is forced, with tragic consequences, to reconcile conflicting impulses in his own nature--a need to serve the protecting deity of his Umuaro peop= a desire to retain control over their religious observances; and a need to gain increased personal power by pushing his authority to the limits. He ultimately fails as he leads his people to their own destruction, and consequently, his personal tragedy arises. Arrow of God is an unforgettable portrayal of the loss of faith, and the downfall of a man in a society forever altered by colonialism.', N'Component/images/book3.png', 145, CAST(N'2022-08-01' AS Date), 3, 3)
INSERT [dbo].[Book] ([book_id], [book_title], [brief], [publisher], [content], [book_img], [viewed], [Release_Date], [category], [author_id]) VALUES (N'B104', N'Monster Night', N'Sed ut perspiciatis', N'Penguin Books', N'By the renowned author ofThings Fall Apart, this novel foreshadows the Nigerian coups of 1966 and shows the color and vivacity as well as the violence and corruption of a society making its own way between the two worlds. This description may be from another edition of this product.', N'Component/images/book4.png', 197, CAST(N'2022-08-01' AS Date), 4, 3)
INSERT [dbo].[Book] ([book_id], [book_title], [brief], [publisher], [content], [book_img], [viewed], [Release_Date], [category], [author_id]) VALUES (N'B105', N'Critical Thinking', N'Sed ut perspiciatis', N'Granta Books', N'For readers who are serious about confronting the big issues in life—but are turned off by books which deal with them through religion, spirituality, or psychobabble, this is an honest, intelligent discussion by a philosopher that doesn''t hide from the difficulties or make undeliverable promises. It aims to help the reader understand the overlooked issues behind the obvious questions, and shows how philosophy does not so much answer them as help provide us with the resources to answer them for ourselves.“Useful and provocative.”—The Wall Street Journal. Looking for a clear guide to what contemporary philosophy has to say about the meaning of life? Baggini takes us through all the plausible answers, weaving together Kierkegaard, John Stuart Mill, Monty Python, and Funkadelic in an entertaining but always carefully reasoned discussion.”—Peter Singer, author of How Are We To Live', N'Component/images/book5.png', 197, CAST(N'2022-08-01' AS Date), 4, 3)
INSERT [dbo].[Book] ([book_id], [book_title], [brief], [publisher], [content], [book_img], [viewed], [Release_Date], [category], [author_id]) VALUES (N'B106', N'Intelligence Silent', N'Sed ut perspiciatis', N'Penguin Publishing Group', N'"A true classic of world literature . . . A masterpiece that has inspired generations of writers in Nigeria, across Africa, and around the world." -- Barack Obama "African literature is incomplete and unthinkable without the works of Chinua Achebe." --Toni Morrison Nominated as one of America''s best-loved novels by PBS''s The Great American Read Things Fall Apart is the first of three novels in Chinua Achebe''s critically acclaimed African Trilogy. It is a classic narrative about Africa''s cataclysmic encounter with Europe as it establishes a colonial presence on the continent. Told through the fictional experiences of Okonkwo, a wealthy and fearless Igbo warrior of Umuofia in the late 1800s, Things Fall Apart explores one man''s futile resistance to the devaluing of his Igbo traditions by British political andreligious forces and his despair as his community capitulates to the powerful new order. With more than 20 million copies sold and translated into fifty-seven languages, Things Fall Apart provides one of the most illuminating and permanent monuments to African experience. Achebe does not only capture life in a pre-colonial African village, he conveys the tragedy of the loss of that world while broadening our understanding of our contemporary realities.', N'Component/images/book6.png', 100, CAST(N'2022-08-01' AS Date), 1, 5)
INSERT [dbo].[Book] ([book_id], [book_title], [brief], [publisher], [content], [book_img], [viewed], [Release_Date], [category], [author_id]) VALUES (N'B107', N'The Magic', N'Sed ut perspiciatis', N'Penguin Publishing Group', N'A classic story of moral struggle in an age of turbulent social change and the final book in Chinua Achebe''s The African Trilogy When Obi Okonkwo, grandson of Okonkwo, the main character in Things Fall Apart returns to Nigeria from England in the 1950s, his foreign education separates him from his African roots. No Longer at Ease, the third and concluding novel in Chinua Achebe''s The African Trilogy , depicts the uncertainties that beset the nation of Nigeria, as independence from colonial rule loomed near. In Obi Okonkwo''s experiences, the ambiguities, pitfalls, and temptations of a rapidly evolving society are revealed. He is part of a ruling Nigerian elite whose corruption he finds repugnant. His fate, however, overtakes him as he finds himself trapped between the expectation of his family, his village--both representations of the traditional world of his ancestors--and the colonial world. A story of a man lost in cultural limbo, and a nation entering a new age of disillusionment, No Longer at Ease is a powerful metaphor for his generation of young Nigerians.', N'Component/images/book7.png', 245, CAST(N'2022-08-01' AS Date), 2, 5)
INSERT [dbo].[Book] ([book_id], [book_title], [brief], [publisher], [content], [book_img], [viewed], [Release_Date], [category], [author_id]) VALUES (N'B108', N'Jesus', N'Sed ut perspiciatis', N'Penguin Publishing Group', N'The second novel in Chinua Achebe''s masterful African trilogy, following Things Fall Apart and preceding No Longer at Ease When Things Fall Apart ends, colonial rule has been introduced to Umuofia, and the character of the nation, its values, freedoms, religious and socio-political foundations have substantially and irrevocably been altered. Arrow of God, the second novel in Chinua Achebe''s The African Trilogy , moves the historical narrative forward. This time, the action revolves around Ezeulu, the headstrong chief priest of the god Ulu, which is worshipped by the six villages of Umuaro. The novel is a meditation on the nature, uses, and responsibility of power and leadership. Ezeulu finds that his authority is increasingly under threat from rivals within his nation and functionaries of the newly established British colonial government. Yet he sees himself as untouchable. He is forced, with tragic consequences, to reconcile conflicting impulses in his own nature--a need to serve the protecting deity of his Umuaro peop= a desire to retain control over their religious observances; and a need to gain increased personal power by pushing his authority to the limits. He ultimately fails as he leads his people to their own destruction, and consequently, his personal tragedy arises. Arrow of God is an unforgettable portrayal of the loss of faith, and the downfall of a man in a society forever altered by colonialism.', N'Component/images/book9.png', 145, CAST(N'2022-08-01' AS Date), 3, 6)
INSERT [dbo].[Book] ([book_id], [book_title], [brief], [publisher], [content], [book_img], [viewed], [Release_Date], [category], [author_id]) VALUES (N'B109', N'Science in the eyes', N'Sed ut perspiciatis', N'Penguin Books', N'By the renowned author ofThings Fall Apart, this novel foreshadows the Nigerian coups of 1966 and shows the color and vivacity as well as the violence and corruption of a society making its own way between the two worlds. This description may be from another edition of this product.', N'Component/images/book10.png', 197, CAST(N'2022-08-01' AS Date), 4, 7)
INSERT [dbo].[Book] ([book_id], [book_title], [brief], [publisher], [content], [book_img], [viewed], [Release_Date], [category], [author_id]) VALUES (N'B110', N'Cinder', N'Sed ut perspiciatis', N' Grand Central Publishing', N'The unforgettable novel of a childhood in a sleepy Southern town and the crisis of conscience that rocked it, To Kill A Mockingbird became both an instant bestseller and a critical success when it was first published in 1960. It went on to win the Pulitzer Prize in 1961 and was later made into an Academy Award-winning film, also a classic.', N'Component/images/book10.png', 197, CAST(N'2022-02-07' AS Date), 4, 7)
INSERT [dbo].[Book] ([book_id], [book_title], [brief], [publisher], [content], [book_img], [viewed], [Release_Date], [category], [author_id]) VALUES (N'B111', N'Caraval', N'Sed ut perspiciatis', N' Skyhorse', N' Mistakes Can Kill You highlights an essential selection featuring nine of LAmours earlier short stories, sometimes written under the pen name Jim Mayo, that exemplify the rugged morality of the best Western writing. In “Black Rock Coffin-Makers,” two men ready to kill over ownership of a ranch get more than they bargain for when a stranger is caught in the crossfire. And in “Four- Card Draw,” Allen Ring wins a ranch in a poker game, only to find out an unsolved murder was committed there years ago and law enforcement thinks Ring knows more about it than hes letting on.', N'Component/images/book11.png', 197, CAST(N'2022-05-09' AS Date), 4, 7)
INSERT [dbo].[Book] ([book_id], [book_title], [brief], [publisher], [content], [book_img], [viewed], [Release_Date], [category], [author_id]) VALUES (N'B112', N'Harry Potter', N'Sed ut perspiciatis', N'Penguin Books', N'Every summer Sam spends a few precious months being human, until the cold causes him to shift to a wolf once again. Discover what happens when he meets Grace in this chilling romance.', N'Component/images/book12.png', 197, CAST(N'2022-12-03' AS Date), 4, 7)
INSERT [dbo].[Book] ([book_id], [book_title], [brief], [publisher], [content], [book_img], [viewed], [Release_Date], [category], [author_id]) VALUES (N'B113', N'The Wicked King', N'Sed ut perspiciatis', N'Penguin Books', N'The dreamers walk among us...and so do the dreamed. Those who dream cannot stop dreaming - they can only try to control it. Those who are dreamed cannot have their own lives - they will sleep forever if their dreamers die. And then there are those who are drawn to the dreamers. To use them. To trap them. To kill them before their dreams destroy us all.', N'Component/images/book13.png', 197, CAST(N'2022-05-12' AS Date), 4, 7)
INSERT [dbo].[Book] ([book_id], [book_title], [brief], [publisher], [content], [book_img], [viewed], [Release_Date], [category], [author_id]) VALUES (N'B114', N'Red Queen ', N'Sed ut perspiciatis', N'Penguin Books', N'It is freezing in the churchyard, even before the dead arrive. Every year, Blue Sargent stands next to her clairvoyant mother as the soon-to-be dead walk past. Blue herself never sees them - not until this year, when a boy emerges from the dark and speaks directly to her. His name is Gansey, and Blue soon discovers that he is a rich student at Aglionby, the local private school. Blue has a policy of staying away from Aglionby boys. Known as Raven Boys, they can only mean trouble.', N'Component/images/book14.png', 197, CAST(N'2022-02-02' AS Date), 4, 7)
INSERT [dbo].[Book] ([book_id], [book_title], [brief], [publisher], [content], [book_img], [viewed], [Release_Date], [category], [author_id]) VALUES (N'B115', N' Throne of Glass', N'Sed ut perspiciatis', N'Penguin Books', N' umans and androids crowd the raucous streets of New Beijing. A deadly plague ravages the population. From space, a ruthless lunar people watch, waiting to make their move. No one knows that Earths fate hinges on one girl....', N'Component/images/book15.png', 197, CAST(N'2022-11-10' AS Date), 4, 7)
GO
INSERT [dbo].[BookCase] ([book_case_id], [book_case_name]) VALUES (12, N'ruacon')
GO
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([category_id], [category_name]) VALUES (1, N'Technology')
INSERT [dbo].[Category] ([category_id], [category_name]) VALUES (2, N'Scientist')
INSERT [dbo].[Category] ([category_id], [category_name]) VALUES (3, N'Math')
INSERT [dbo].[Category] ([category_id], [category_name]) VALUES (4, N'Drama')
INSERT [dbo].[Category] ([category_id], [category_name]) VALUES (5, N'Historical')
INSERT [dbo].[Category] ([category_id], [category_name]) VALUES (6, N'Mystery')
INSERT [dbo].[Category] ([category_id], [category_name]) VALUES (7, N'Action')
INSERT [dbo].[Category] ([category_id], [category_name]) VALUES (8, N'Romance')
SET IDENTITY_INSERT [dbo].[Category] OFF
GO
SET IDENTITY_INSERT [dbo].[ContactUs] ON 

INSERT [dbo].[ContactUs] ([contact_id], [contact_name], [contact_email], [contact_phone], [contact_mess]) VALUES (7, N'long', N'ruacon742000@gmail.com', N'0123456789', N' Are easy to find, so a visitor can quickly get in touch with you.')
INSERT [dbo].[ContactUs] ([contact_id], [contact_name], [contact_email], [contact_phone], [contact_mess]) VALUES (8, N'long', N'ducnguyen0809@gmail.com', N'0123456789', N' Explain why someone should contact your business.')
INSERT [dbo].[ContactUs] ([contact_id], [contact_name], [contact_email], [contact_phone], [contact_mess]) VALUES (9, N'long', N'duypham@gmail.com', N'0123456789', N' Describe how your business can help solve the visitors problems.')
INSERT [dbo].[ContactUs] ([contact_id], [contact_name], [contact_email], [contact_phone], [contact_mess]) VALUES (10, N'long', N'nguyenthithienkim18092005@gmail.com', N'0123456789', N' Include an email and phone number so visitors can get in touch with you on their first attempt.')
INSERT [dbo].[ContactUs] ([contact_id], [contact_name], [contact_email], [contact_phone], [contact_mess]) VALUES (11, N'long', N'minhnguyen0212@gmail.com', N'0123456789', N' Include a short form using fields that ll help your business understand whos contacting them.')
INSERT [dbo].[ContactUs] ([contact_id], [contact_name], [contact_email], [contact_phone], [contact_mess]) VALUES (12, N'long', N'trantuanminh12022000@gmail.com', N'0123456789', N' Have a call-to-action to provide visitors with another action to take if they choose not to complete the form.')
INSERT [dbo].[ContactUs] ([contact_id], [contact_name], [contact_email], [contact_phone], [contact_mess]) VALUES (13, N'long', N'dangthinhuy01012000@gmail.com', N'0123456789', N' Showcase the companys thought leadership, whether by including a list of recent blog posts or articles about the company in the press.')
INSERT [dbo].[ContactUs] ([contact_id], [contact_name], [contact_email], [contact_phone], [contact_mess]) VALUES (14, N'long', N'phamduchuy13021998@gmail.com', N'0123456789', N' Link to active social media accounts like Twitter, Facebook, Instagram, and LinkedIn to give visitors another way to engage with the business.')
INSERT [dbo].[ContactUs] ([contact_id], [contact_name], [contact_email], [contact_phone], [contact_mess]) VALUES (15, N'long', N'nguyenthihien14052000@gmail.com', N'0123456789', N' Redirect to a thank you page that explains when and how youll be contacting the visitor.')
INSERT [dbo].[ContactUs] ([contact_id], [contact_name], [contact_email], [contact_phone], [contact_mess]) VALUES (16, N'long', N'dangthitructhi@gmail.com', N'0123456789', N' Promote helpful content and resources.')
INSERT [dbo].[ContactUs] ([contact_id], [contact_name], [contact_email], [contact_phone], [contact_mess]) VALUES (17, N'long', N'nguyenhuuphu@gmail.com', N'0123456789', N' Are creative and memorable, allowing visitors to associate contacting your brand with a positive or funny memory.')
INSERT [dbo].[ContactUs] ([contact_id], [contact_name], [contact_email], [contact_phone], [contact_mess]) VALUES (18, N'long', N'trantuannamlong@gmail.com', N'0123456789', N' Show off what your brand does so visitors and potential customers can get a sense of the work you do before they even get in touch.')
INSERT [dbo].[ContactUs] ([contact_id], [contact_name], [contact_email], [contact_phone], [contact_mess]) VALUES (19, N'long', N'truongthiphuong@gmail.com', N'0123456789', N' Avoid unnecessary fields and words, so your page remains as straightforward as possible — no fluff.')
INSERT [dbo].[ContactUs] ([contact_id], [contact_name], [contact_email], [contact_phone], [contact_mess]) VALUES (20, N'long', N'vuhuuky@gmail.com', N'0123456789', N' The purpose of your contact us page is one of the most direct.')
INSERT [dbo].[ContactUs] ([contact_id], [contact_name], [contact_email], [contact_phone], [contact_mess]) VALUES (21, N'long', N'nguyenducthai@gmail.com', N'0123456789', N' Sometimes users want to talk to you on the phone, or live chat, rather than fill out a form.')
SET IDENTITY_INSERT [dbo].[ContactUs] OFF
GO
INSERT [dbo].[Role] ([role_id], [authority]) VALUES (1, 1)
INSERT [dbo].[Role] ([role_id], [authority]) VALUES (2, 2)
INSERT [dbo].[Role] ([role_id], [authority]) VALUES (3, 0)
INSERT [dbo].[Role] ([role_id], [authority]) VALUES (12, 0)
INSERT [dbo].[Role] ([role_id], [authority]) VALUES (13, 0)
INSERT [dbo].[Role] ([role_id], [authority]) VALUES (14, 0)
GO
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([id], [user_name], [password], [email], [hash], [active], [phone], [dob], [gender], [image]) VALUES (1, N'admin', N'admin', N'admin@gmail.com', NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Users] ([id], [user_name], [password], [email], [hash], [active], [phone], [dob], [gender], [image]) VALUES (2, N'xvcv', N'xcvxcv', N'xcv@gmail.com', NULL, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Users] ([id], [user_name], [password], [email], [hash], [active], [phone], [dob], [gender], [image]) VALUES (3, N'egbc', N'cdfbv', N'cdfbv@gmail.com', N'68298342ca438f15f4de3d76ff34ee1d', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Users] ([id], [user_name], [password], [email], [hash], [active], [phone], [dob], [gender], [image]) VALUES (4, N'egbc', N'cdfbv', N'cdfbv@gmail.com', N'68298342ca438f15f4de3d76ff34ee1d', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Users] ([id], [user_name], [password], [email], [hash], [active], [phone], [dob], [gender], [image]) VALUES (5, N'egbc', N'cdfbv', N'cdfbv@gmail.com', N'68298342ca438f15f4de3d76ff34ee1d', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Users] ([id], [user_name], [password], [email], [hash], [active], [phone], [dob], [gender], [image]) VALUES (6, N'sddasd', N'cdfbv', N'cdvv@gmail.com', N'161384db9a35f89eb30fdfa40d50da75', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Users] ([id], [user_name], [password], [email], [hash], [active], [phone], [dob], [gender], [image]) VALUES (7, N'egbc', N'cdfbv', N'cdvv@gmail.com', N'161384db9a35f89eb30fdfa40d50da75', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Users] ([id], [user_name], [password], [email], [hash], [active], [phone], [dob], [gender], [image]) VALUES (8, N'adbfb', N'cdfbv', N'cdvv@gmail.com', N'894f83de09d1cdc348b199c8af0051aa', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Users] ([id], [user_name], [password], [email], [hash], [active], [phone], [dob], [gender], [image]) VALUES (12, N'ruacon', N'long2000', N'longnthe140165@gmail.com', N'39d7e6e43cc72a7738a91009c6da44e4', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Users] ([id], [user_name], [password], [email], [hash], [active], [phone], [dob], [gender], [image]) VALUES (13, N'test', N'test', N'test@gmail.com', N'e915df84c7261db5d6e4dc94d064d999', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Users] ([id], [user_name], [password], [email], [hash], [active], [phone], [dob], [gender], [image]) VALUES (14, N'test2', N'long2000', N'test2@gmail.com', N'15ef2b64ca4447506a0587e91260d97e', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Users] OFF
GO
ALTER TABLE [dbo].[Users] ADD  DEFAULT ('0') FOR [active]
GO
ALTER TABLE [dbo].[Book]  WITH CHECK ADD FOREIGN KEY([author_id])
REFERENCES [dbo].[Author] ([author_id])
GO
ALTER TABLE [dbo].[Book]  WITH CHECK ADD FOREIGN KEY([category])
REFERENCES [dbo].[Category] ([category_id])
GO
ALTER TABLE [dbo].[BookCase]  WITH CHECK ADD FOREIGN KEY([book_case_id])
REFERENCES [dbo].[Users] ([id])
GO
ALTER TABLE [dbo].[Contain]  WITH CHECK ADD FOREIGN KEY([book_case_id])
REFERENCES [dbo].[BookCase] ([book_case_id])
GO
ALTER TABLE [dbo].[Contain]  WITH CHECK ADD FOREIGN KEY([book_id])
REFERENCES [dbo].[Book] ([book_id])
GO
ALTER TABLE [dbo].[Role]  WITH CHECK ADD FOREIGN KEY([role_id])
REFERENCES [dbo].[Users] ([id])
GO
