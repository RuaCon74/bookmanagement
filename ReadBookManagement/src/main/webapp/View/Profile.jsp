<%@page import="fa.training.entities.Users"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Book Library - Book Guide Author, Publication and Store</title>
        <!-- CUSTOM STYLE -->
        <link href="Component/css/style.css" rel="stylesheet">
        <!-- THEME TYPO -->
        <link href="Component/css/themetypo.css" rel="stylesheet">
        <!-- BOOTSTRAP -->
        <link href="Component/css/bootstrap.css" rel="stylesheet">
        <!-- COLOR FILE -->
        <link href="Component/css/color.css" rel="stylesheet">
        <!-- FONT AWESOME -->
        <link href="Component/css/font-awesome.min.css" rel="stylesheet">
        <!-- BX SLIDER -->
        <link href="Component/css/jquery.bxslider.css" rel="stylesheet">
        <!-- Boostrap Slider -->
        <link href="Component/css/bootstrap-slider.css" rel="stylesheet">
        <!-- Widgets -->
        <link href="Component/css/widget.css" rel="stylesheet">
        <!-- Owl Carusel -->
        <link href="Component/css/owl.carousel.css" rel="stylesheet">
        <!-- responsive -->
        <link href="Component/css/responsive.css" rel="stylesheet">
        <!-- Component -->
        <link href="Component/js/dl-menu/component.css" rel="stylesheet">
        <!-- Font-Awesome -->
        <script src="https://kit.fontawesome.com/db62dd06f6.js"
        crossorigin="anonymous"></script>

    </head>
    <body>
        <div id="loader-wrapper">
            <div id="loader"></div>

            <div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>

        </div>
        <!--WRAPPER START-->
        <div class="wrapper kode-header-class-3">
            <jsp:include page="Header.jsp"></jsp:include>
                <!--CONTENT START-->
                <div class="kode-content padding-tb-50">
                    <div class="container">
                        <c:if test="${user != null}">
                        <table>
                            <tr>
                                <td>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-container">
                                            Username
                                        </div>
                                    </div>
                                </td>
                                <td>${user.username }</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-container">
                                            Email
                                        </div>
                                    </div>
                                </td>
                                <td>${user.email }</td>
                            </tr>
                            </table>
                         </c:if>
                        
                    </div>
                </div>
            </div>
        <jsp:include page="Footer.jsp"></jsp:include>
    
    <!--WRAPPER END-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="Component/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="Component/js/bootstrap.min.js"></script>
    <script src="Component/js/jquery.bxslider.min.js"></script>
    <script src="Component/js/bootstrap-slider.js"></script>
    <script src="Component/js/waypoints.min.js"></script>
    <script src="Component/js/jquery.counterup.min.js"></script>
    <script src="Component/js/owl.carousel.js"></script>
    <script
    src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
    <script src="Component/js/dl-menu/modernizr.custom.js"></script>
    <script src="Component/js/dl-menu/jquery.dlmenu.js"></script>
    <script src="Component/js/functions.js"></script>

</body>
</html>