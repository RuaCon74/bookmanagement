<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Book Library - Book Guide Author, Publication and Store</title>
<!-- CUSTOM STYLE -->
<link href="Component/css/style.css" rel="stylesheet">
<!-- THEME TYPO -->
<link href="Component/css/themetypo.css" rel="stylesheet">
<!-- BOOTSTRAP -->
<link href="Component/css/bootstrap.css" rel="stylesheet">
<!-- COLOR FILE -->
<link href="Component/css/color.css" rel="stylesheet">
<!-- FONT AWESOME -->
<link href="Component/css/font-awesome.min.css" rel="stylesheet">
<!-- BX SLIDER -->
<link href="Component/css/jquery.bxslider.css" rel="stylesheet">

<link href="Component/css/bootstrap-slider.css" rel="stylesheet">

<link href="Component/css/widget.css" rel="stylesheet">

<link href="Component/css/shortcode.css" rel="stylesheet">
<!-- responsive -->
<link href="Component/css/responsive.css" rel="stylesheet">
<!-- Component -->
<link href="Component/js/dl-menu/component.css" rel="stylesheet">
<!-- Font-Awesome -->
<script src="https://kit.fontawesome.com/db62dd06f6.js"
	crossorigin="anonymous"></script>

</head>
<body>
	<div id="loader-wrapper">
		<div id="loader"></div>

		<div class="loader-section section-left"></div>
		<div class="loader-section section-right"></div>

	</div>
	<!--WRAPPER START-->
	<div class="wrapper kode-header-class-3">
		<jsp:include page="Header.jsp"></jsp:include>
		<!--CONTENT START-->
		<div class="kode-content padding-tb-50">
			<div class="container">
				<div class="row">
					<div class="col-md-3 sidebar">
						<!--SEARCH WIDGET START-->
						<div class="widget widget-search">
							<h2>Search</h2>
							<div class="input-container">
								<input type="text" placeholder="Enter Keyword"> <i
									class="fa fa-search"></i>
							</div>
						</div>
						<!--SEARCH WIDGET END-->

					</div>
					<div class="col-md-9">
						<div class="row">
							<!--BOOK LISTING START-->
							<c:forEach items="${listBook}" var="b">
								<!--BOOK LISTING START-->
								<div class="col-md-4 col-sm-6">
									<div class="books-listing-4">
										<div class="book_id">
											<input type="hidden" name="bookId" value="${b.bookId}" />
										</div>
										<div class="kode-thumb">
											<a href="BookDetail.jsp"><img src="${b.bookImg}" alt=""></a>
										</div>
										<div class="kode-text">
											<h3>
												<a href="BookDetail.jsp">${b.bookTitle}</a>
											</h3>
											<p>${b.brief}</p>
										</div>
										<a href="remove?bookId=${b.bookId}" class="add-to-cart"><i
											class="fa-solid fa-heart"></i> Remove from favorite</a>
									</div>
								</div>

							</c:forEach>
							<!--BOOK LISTING END-->
							<nav>
								<ul class="pagination">
									<li><a href="#" aria-label="Previous"> <span
											aria-hidden="true">&laquo;</span>
									</a></li>
									<c:forEach begin="1" end="${endPage}" var="i">

										<li><a class="${tag==i?"
											activ":""}" href="viewBookCase?index=${i}">${i}</a></li>
									</c:forEach>
									<li><a href="#" aria-label="Next"> <span
											aria-hidden="true">&raquo;</span>
									</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>

			</div>
		</div>
		<jsp:include page="Footer.jsp"></jsp:include>
	</div>
	<!--WRAPPER END-->
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="Component/js/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="Component/js/bootstrap.min.js"></script>
	<script src="Component/js/dl-menu/modernizr.custom.js"></script>
	<script src="Component/js/dl-menu/jquery.dlmenu.js"></script>
	<script src="Component/js/jquery.bxslider.min.js"></script>
	<script src="Component/js/bootstrap-slider.js"></script>
	<script src="Component/js/waypoints.min.js"></script>
	<script src="Component/js/jquery.counterup.min.js"></script>
	<script src="Component/js/functions.js"></script>
</body>
</html>