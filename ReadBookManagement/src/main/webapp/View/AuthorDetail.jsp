<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Book Library - Book Guide Author, Publication and Store</title>
<!-- CUSTOM STYLE -->
<link href="Component/css/style.css" rel="stylesheet">
<!-- THEME TYPO -->
<link href="Component/css/themetypo.css" rel="stylesheet">
<!-- SHORTCODES -->
<link href="Component/css/shortcode.css" rel="stylesheet">
<!-- BOOTSTRAP -->
<link href="Component/css/bootstrap.css" rel="stylesheet">
<!-- COLOR FILE -->
<link href="Component/css/color.css" rel="stylesheet">
<!-- FONT AWESOME -->
<link href="Component/css/font-awesome.min.css" rel="stylesheet">
<!-- BX SLIDER -->
<link href="Component/css/jquery.bxslider.css" rel="stylesheet">
<!-- Boostrap Slider -->
<link href="Component/css/bootstrap-slider.css" rel="stylesheet">
<!-- Widgets -->
<link href="Component/css/widget.css" rel="stylesheet">
<!-- Owl Carusel -->
<link href="Component/css/owl.carousel.css" rel="stylesheet">
<!-- responsive -->
<link href="Component/css/responsive.css" rel="stylesheet">
<!-- Component -->
<link href="Component/js/dl-menu/component.css" rel="stylesheet">
<!-- Font-Awesome -->
<script src="https://kit.fontawesome.com/db62dd06f6.js"
	crossorigin="anonymous"></script>
</head>
<body>
	<div id="loader-wrapper">
		<div id="loader"></div>

		<div class="loader-section section-left"></div>
		<div class="loader-section section-right"></div>

	</div>
	<!--WRAPPER START-->
	<div class="wrapper kode-header-class-3">
		<jsp:include page="Header.jsp"></jsp:include>
		<!--CONTENT START-->
		<div class="kode-content">
			<!--AUTHOR DETAIL SECTION START-->
			<section class="kode-author-detail-2">
				<div class="container">
					<div class="kode-thumb">
						<img src="${authorDetail.authorImg}" alt="">
					</div>
					<div class="kode-text">
						<h2>${authorDetail.authorName }</h2>
						<h5>Co-Founder &amp; Author</h5>
						<div class="contact-box">
							<div class="row">
								<div class="col-md-8">
									<table>
										<tr>
											<td><i class="fa fa-phone"></i></td>
											<td>Phone No:</td>
											<td>${authorDetail.phone}</td>
										</tr>
										<tr>
											<td><i class="fa fa-envelope-o"></i></td>
											<td>Email ID:</td>
											<td>${authorDetail.email}</td>
										</tr>
										<tr>
											<td><i class="fa fa-fax"></i></td>
											<td>Fax No:</td>
											<td>333-365-9874</td>
										</tr>
									</table>
								</div>
								<div class="col-md-4">
									<div class="social-icon">
										<ul>
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
											<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
											<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
											<li><a href="#"><i class="fa fa-vimeo"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<p>${authorDetail.authorDesc}</p>
						<div class="signature">
							<img src="Component/images/signature-1.png" alt="">
						</div>
					</div>
				</div>
			</section>
			<!--AUTHOR DETAIL SECTION END-->
			<!--KODE BIOGRAPHY SECTION START-->
			<section class="kode-bio">
				<div class="container">
					<div class="section-heading-1">
						<h2>Biography</h2>
						<div class="kode-icon">
							<i class="fa fa-book"></i>
						</div>
					</div>
					<div class="kode-text">
						<p>Pages you view in incognito tabs won’t stick around in
							your browser’s history, cookie store, or search history after
							you’ve closed all of your incognito tabs. Any files you
							download or bookmarks you create will be kept cookie store, or
							search history after you’ve closed all of your incognito tabs.
							cookie store, or search history after you’ve closed all of your
							incognito tabs. in your browser’s history, cookie store, or
							search history after you’ve closed all of your incognito tabs</p>
					</div>
					<div class="kode-text">
						<div class="row">
							<div class="col-md-6">
								<h2>Early Life and Education</h2>
								<p>Pages you view in incognito tabs won’t stick around in
									your browser’s history, cookie store, or search history after
									you’ve closed all of your incognito tabs. Any files you
									download or bookmarks you create will be kept cookie store, or
									search history after you’ve closed all of your incognito
									tabs. cookie store, or search history after you’ve closed all
									of your incognito tabs. in your browser’s history, cookie
									store, or search history after you’ve closed all of your
									incognito tabs</p>
							</div>
							<div class="col-md-6">
								<h2>Early Life and Education</h2>
								<p>Pages you view in incognito tabs won’t stick around in
									your browser’s history, cookie store, or search history after
									you’ve closed all of your incognito tabs. Any files you
									download or bookmarks you create will be kept cookie store, or
									search history after you’ve closed all of your incognito
									tabs. cookie store, or search history after you’ve closed all
									of your incognito tabs. in your browser’s history, cookie
									store, or search history after you’ve closed all of your
									incognito tabs</p>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!--KODE BIOGRAPHY SECTION END-->
			<!--RECENT RELEASE SECTION START-->
			<section class="recent-release">
				<div class="container">
					<div class="section-heading-1">
						<h2>Published Books</h2>
						<div class="kode-icon">
							<i class="fa fa-book"></i>
						</div>
					</div>
					<div class="owl-release owl-theme">
						<c:forEach items="${listBookOfAuthor}" var="o">
							<div class="item">
								<div class="book-released">
									<div class="kode-thumb">
										<a href="#"><img src="${o.bookImg }" alt=""></a>
										<div class="cart-btns">
											<a href="#" data-toggle="tooltip" title="${o.viewed }"><i
												class="fa fa-eye"></i></a> <a href="#" data-toggle="tooltip"
												title="Add To Cart"><i class="fa fa-shopping-cart"></i></a>
											<a href="#" data-toggle="tooltip" title="Add To Wishlist"><i
												class="fa fa-heart-o"></i></a>
										</div>
									</div>
									<div class="kode-text">
										<h3>${o.bookTitle}</h3>

									</div>
								</div>
							</div>
						</c:forEach>
					</div>
				</div>
			</section>
			<!--RECENT RELEASE SECTION END-->
			<!--COUNT UP SECTION START-->
			<div class="lib-count-up-section">
				<div class="container">
					<div class="row">
						<div class="col-md-3 col-sm-6">
							<div class="count-up">
								<span class="counter circle">21</span>
								<p>Working Year</p>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="count-up">
								<span class="counter circle">8589</span>
								<p>Books Sold</p>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="count-up">
								<span class="counter circle">458</span>
								<p>Top Author</p>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="count-up">
								<span class="counter circle">750</span>
								<p>Book Published</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--COUNT UP SECTION END-->

			<!--GIFT CARD SECTION START-->
			<section class="lib-textimonials">
				<div class="container">
					<!--SECTION HEADING START-->
					<div class="section-heading-1 dark-sec">
						<h2>Our Testimonials</h2>
						<p>What our clients say about the books reviews and comments</p>
						<div class="kode-icon">
							<i class="fa fa-book"></i>
						</div>
					</div>
					<!--SECTION HEADING END-->
					<div class="owl-testimonials owl-theme">
						<!--BLOG ITEM START-->
						<div class="item">
							<div class="lib-testimonial-content">
								<div class="kode-text">
									<p>I loved thrift books! It's refreshing to buy discounted
										books and have them shipped quickly. I could afford to buy 3
										copies to hand out to friends also interested in the topic.
										Thank you!! Read more</p>
								</div>
								<div class="kode-thumb">
									<img src="Component/images/testimonials1.png" alt="">
								</div>
								<h4>Jenifer Robbert</h4>
								<p class="title">Author</p>
							</div>
						</div>
						<!--BLOG ITEM END-->
						<!--BLOG ITEM START-->
						<div class="item">
							<div class="lib-testimonial-content">
								<div class="kode-text">
									<p>You have great prices and the books are in the shape as
										stated. Although it takes so long for them to get to their
										destination. I have been ordering for years and get great
										books in the shape said.</p>
								</div>
								<div class="kode-thumb">
									<img src="Component/images/testimonials-img4.png" alt="">
								</div>
								<h4>Jenifer Robbert</h4>
								<p class="title">Author</p>
							</div>
						</div>
						<!--BLOG ITEM END-->
						<!--BLOG ITEM START-->
						<div class="item">
							<div class="lib-testimonial-content">
								<div class="kode-text">
									<p>I have made many orders with Thrift Books. I always get
										exactly what I order in a timely manner at a great price. I
										have had to contact the customer service team once.</p>
								</div>
								<div class="kode-thumb">
									<img src="Component/images/testimonials-img3.png" alt="">
								</div>
								<h4>Jenifer Robbert</h4>
								<p class="title">Author</p>
							</div>
						</div>
						<!--BLOG ITEM END-->
						<!--BLOG ITEM START-->
						<div class="item">
							<div class="lib-testimonial-content">
								<div class="kode-text">
									<p>I couldn't believe the prices for such great books, at
										no shipping! I am going to be a good customer at your store!
										And, I am telling my Facebook friends about.</p>
								</div>
								<div class="kode-thumb">
									<img src="Component/images/testimonials-img2.png" alt="">
								</div>
								<h4>Jenifer Robbert</h4>
								<p class="title">Author</p>
							</div>
						</div>
						<!--BLOG ITEM END-->
						<!--BLOG ITEM START-->
						<div class="item">
							<div class="lib-testimonial-content">
								<div class="kode-text">
									<p>ordered 14 books, received 14 books within a week. very
										happy with customer support and with the receipt of books.
										Keep It Up Good Guide we love you the best books library
										available today.</p>
								</div>
								<div class="kode-thumb">
									<img src="Component/images/writer2.png" alt="">
								</div>
								<h4>Jenifer Robbert</h4>
								<p class="title">Author</p>
							</div>
						</div>
						<!--BLOG ITEM END-->
						<!--BLOG ITEM START-->
						<div class="item">
							<div class="lib-testimonial-content">
								<div class="kode-text">
									<p>Thrift Books is the absolute best book seller on the
										Internet!! Their selection is marvelous, price/shipping
										unbeatable and timely service is outstanding.</p>
								</div>
								<div class="kode-thumb">
									<img src="Component/images/writer3.png" alt="">
								</div>
								<h4>Jenifer Robbert</h4>
								<p class="title">Author</p>
							</div>
						</div>
						<!--BLOG ITEM END-->
					</div>
				</div>
			</section>
			<!--GIFT CARD SECTION END-->
		</div>
		<jsp:include page="Footer.jsp"></jsp:include>
	</div>
	<!--WRAPPER END-->
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="Component/js/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="Component/js/bootstrap.min.js"></script>
	<script src="Component/js/dl-menu/modernizr.custom.js"></script>
	<script src="Component/js/dl-menu/jquery.dlmenu.js"></script>
	<script src="Component/js/jquery.bxslider.min.js"></script>
	<script src="Component/js/bootstrap-slider.js"></script>
	<script src="Component/js/waypoints.min.js"></script>
	<script src="Component/js/jquery.counterup.min.js"></script>
	<script src="Component/js/owl.carousel.js"></script>
	<script src="Component/js/functions.js"></script>
</body>
</html>