<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<!--FACTS SECTION END-->
	<section class="kode-uptodate">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<h2>stay up-to-dated</h2>
				</div>
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-3">
							<div class="social-icons">
								<ul>
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
								</ul>
							</div>
						</div>
						<div class="col-md-9">
							<div class="input-container">
								<form action="sendMail" method="POST">
									<input type="text" placeholder="Your E-mail Address" id="sub-2" name="email">
									<button type="submit">Subscribe</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
    <!--CONTENT END-->
    <footer class="footer-3">
        <div class="container">
            <div class="row">
                <!--CATEGORY WIDGET START-->
                <div class="col-md-3 col-sm-6">
                    <div class="widget widget-categories">
                        <h2>Information</h2>
                        <ul>
                            <li><a href="#">Specials</a></li> 
                            <li><a href="#">New products</a></li> 
                            <li><a href="#">Best sellers</a></li> 
                            <li><a href="#">Contact us</a></li> 
                            <li><a href="#">Terms of use</a></li> 
                            <li><a href="#">Sitemap</a></li>  
                        </ul>
                    </div>
                </div>
                <!--CATEGORY WIDGET END-->
                <!--LATEST NEWS WIDGET START-->
                <div class="col-md-3 col-sm-6">
                    <div class="widget widget-latest-news">
                        <h2>Lates News</h2>
                        <ul>
                            <li>
                                <div class="kode-thumb"><a href="#"><img src="Component/images/latest-news.png" alt=""></a></div>
                                <div class="kode-text">
                                    <p>Phasellus risusa Aliowm</p>
                                    <p>14 December 2015</p>
                                </div>
                            </li>
                            <li>
                                <div class="kode-thumb"><a href="#"><img src="Component/images/latest-news.png" alt=""></a></div>
                                <div class="kode-text">
                                    <p>Phasellus risusa Aliowm</p>
                                    <p>14 December 2015</p>
                                </div>
                            </li>
                            <li>
                                <div class="kode-thumb"><a href="#"><img src="Component/images/latest-news.png" alt=""></a></div>
                                <div class="kode-text">
                                    <p>Phasellus risusa Aliowm</p>
                                    <p>14 December 2015</p>
                                </div>
                            </li>
                            <li>
                                <div class="kode-thumb"><a href="#"><img src="Component/images/latest-news.png" alt=""></a></div>
                                <div class="kode-text">
                                    <p>Phasellus risusa Aliowm</p>
                                    <p>14 December 2015</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--LATEST NEWS WIDGET END-->
                <!--TWITTER WIDGET START-->
                <div class="col-md-3 col-sm-6">
                    <div class="widget widget-flickr">
                        <h2>Flickr Gallery </h2>
                        <ul>
                            <li>
                                <a href="#"><img src="Component/images/gallery1.png" alt=""></a>
                            </li>
                            <li>
                                <a href="#"><img src="Component/images/gallery2.png" alt=""></a>
                            </li>
                            <li>
                                <a href="#"><img src="Component/images/gallery3.png" alt=""></a>
                            </li>
                            <li>
                                <a href="#"><img src="Component/images/gallery4.png" alt=""></a>
                            </li>
                            <li>
                                <a href="#"><img src="Component/images/gallery5.png" alt=""></a>
                            </li>
                            <li>
                                <a href="#"><img src="Component/images/gallery6.png" alt=""></a>
                            </li>
                            <li>
                                <a href="#"><img src="Component/images/gallery4.png" alt=""></a>
                            </li>
                            <li>
                                <a href="#"><img src="Component/images/gallery5.png" alt=""></a>
                            </li>
                            <li>
                                <a href="#"><img src="Component/images/gallery6.png" alt=""></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--TWITTER WIDGET END-->
                
                <!--NEWSLETTER START-->
                <div class="col-md-3 col-sm-6">
                    <div class="widget widget-contact-info">
                        <h2>get in touch</h2>
                        <ul>
                            <li>
                                <i class="fa fa-paper-plane"></i>
                                <div class="kode-text">
                                    <h4>Address</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor. </p>
                                </div>
                            </li>
                            <li>
                                <i class="fa fa-phone"></i>
                                <div class="kode-text">
                                    <h4>phone Number</h4>
                                    <p>+55(62) 55258-4570</p>
                                    <p>+55(62) 55258-4570</p>
                                </div>
                            </li>
                            <li>
                                <i class="fa fa-envelope-o"></i>
                                <div class="kode-text">
                                    <h4>Email Address</h4>
                                    <a href="#">Info@thelibrary.com</a>
                                </div>
                            </li>
                        </ul>                        
                    </div>
                </div>
                <!--NEWSLETTER START END-->
            </div>
        </div>
    </footer>
    <div class="copyrights">
    	<div class="container">
        	<p>Copyrights � 2015-16 KodeForest. All rights reserved</p>
            <div class="cards"><img src="Component/images/cards.png" alt=""></div>
        </div>
    </div>
</body>
</html>