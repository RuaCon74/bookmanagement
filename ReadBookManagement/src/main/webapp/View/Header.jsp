<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<!--HEADER START-->
	<header class="header-3">
		<div class="container">
			<div class="logo-container">
				<div class="row">
					<div class="col-md-3">
						<!--LOGO START-->
						<div class="logo">
							<a href="#"><img src="Component/images/logo-2.png" alt=""></a>
						</div>
						<!--LOGO END-->
					</div>
					<div class="col-md-9">
						<div class="top-strip">
							<div class="pull-left">
								<p>Welcome to Library</p>
							</div>
							<c:if test="${sessionScope.data_session == null }">
								<div class="social-icon">
									<a href="login" class="pull-left">Login</a> <a
										href="register.jsp" class="pull-left">Register</a>
								</div>
							</c:if>
							<c:if test="${sessionScope.data_session != null }">
									<div class="social-icon">
										<li class="nav-item dropdown no-arrow" style="text-decoration: none;"><a
											class="nav-link dropdown-toggle" href="#" id="userDropdown"
											role="button" data-toggle="dropdown" aria-haspopup="true"
											aria-expanded="false" > <span
												class="mr-2 d-none d-lg-inline text-gray-600 small">Hi, ${sessionScope.data_session.username}</span> </a>
												<!-- Dropdown - User Information -->
											<div
												class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
												aria-labelledby="userDropdown">
												<a class="dropdown-item" href="profile" style="margin: 10px; color: orange;"> <i
													class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
													Profile
												</a>
												<c:if test="${sessionScope.role.roleId == 1 }">
													<a class="dropdown-item" href="admin" style="margin: 10px; color: orange;"> <i class="fa-solid fa-folder-gear"></i>
													Admin
													</a>
												</c:if>
												<div class="dropdown-divider"></div>
												<a class="dropdown-item" href="login" style="margin: 10px; color: orange;"> <i
													class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
													Logout
												</a>
											</div>
										</li>
									</div>
								</c:if>
						</div>
						<div class="kode-navigation">
							<ul>
								<li><a href="home">Home</a></li>
								<li><a href="aboutUs">About Us</a></li>
								<li><a href="book">Our Books</a></li>
								<li><a href="author_list">Authors</a></li>
										<c:if test="${sessionScope.data_session != null }">
											<li><a href="viewBookCase">Book Case</a></li>
										</c:if>
										<c:if test="${sessionScope.data_session == null }">
											<li><a href="login">Book Case</a></li>
										</c:if>
								<li class="last"><a href="contactUs">Contact Us</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<!--HEADER END-->
	<!--BANNER START-->
	<div class="kode-inner-banner">
		<div class="kode-page-heading">
			<c:if test="${title != null}">
				<h2>${title}</h2>

				<ol class="breadcrumb">
					<li><a href="home">Home</a></li>
					<li><a href="#">Library</a></li>
					<li class="active">${title}</li>
				</ol>
			</c:if>
		</div>
	</div>
	<!--BANNER END-->
	<!--BUT NOW START-->
	<div class="search-section">
		<div class="container">

			<!-- Tab panes -->
			<div role="tabpanel" class="tab-pane" id="Publications">
				<div class="form-container">
					<div class="row">
						<form action="search" method="get">
							<div class="col-md-4 col-sm-4">
								<input type="text" placeholder="Search" name="search">
							</div>
							<div class="col-md-4 col-sm-4">
								<select name="item" id="cars">
									<option value="0">--Choose filter--</option>
									<option value="book_title">Book title</option>
									<option value="author_name">Author</option>
								</select>
							</div>
							<div class="col-md-4 col-sm-12">
								<button type="submit">Search</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	<!--BUT NOW END-->
</body>
</html>