<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Book Library - Book Guide Author, Publication and Store</title>
<!-- CUSTOM STYLE -->
<link href="Component/css/style.css" rel="stylesheet">
<!-- THEME TYPO -->
<link href="Component/css/themetypo.css" rel="stylesheet">
<!-- BOOTSTRAP -->
<link href="Component/css/bootstrap.css" rel="stylesheet">
<!-- COLOR FILE -->
<link href="Component/css/color.css" rel="stylesheet">
<!-- FONT AWESOME -->
<link href="Component/css/font-awesome.min.css" rel="stylesheet">
<!-- BX SLIDER -->
<link href="Component/css/jquery.bxslider.css" rel="stylesheet">
<!-- Boostrap Slider -->
<link href="Component/css/bootstrap-slider.css" rel="stylesheet">
<!-- Widgets -->
<link href="Component/css/widget.css" rel="stylesheet">
<!-- Owl Carusel -->
<link href="Component/css/owl.carousel.css" rel="stylesheet">
<!-- responsive -->
<link href="Component/css/responsive.css" rel="stylesheet">
<!-- Component -->
<link href="Component/js/dl-menu/component.css" rel="stylesheet">

<link rel="stylesheet" type="text/css"
	href="Component/css/bookblock.css" />
<script src="https://kit.fontawesome.com/db62dd06f6.js"
	crossorigin="anonymous"></script>
</head>
<body>
	<div id="loader-wrapper">
		<div id="loader"></div>

		<div class="loader-section section-left"></div>
		<div class="loader-section section-right"></div>

	</div>
	<!--WRAPPER START-->
	<div class="wrapper kode-header-class-3">
		<!--HEADER START-->
		<header class="header-3">
			<div class="container">
				<div class="logo-container">
					<div class="row">
						<div class="col-md-3">
							<!--LOGO START-->
							<div class="logo">
								<a href="home"><img src="Component/images/logo-2.png" alt=""></a>
							</div>
							<!--LOGO END-->
						</div>
						<div class="col-md-9">
							<div class="top-strip">
								<div class="pull-left">
									<p>Welcome to Library</p>
								</div>
								<c:if test="${sessionScope.data_session == null }">
									<div class="social-icon">
										<a href="login" class="pull-left">Login</a> <a
											href="register" class="pull-left">Register</a>
									</div>
								</c:if>
								<c:if test="${sessionScope.data_session != null }">
									<div class="social-icon">
										<li class="nav-item dropdown no-arrow" style="text-decoration: none;"><a
											class="nav-link dropdown-toggle" href="#" id="userDropdown"
											role="button" data-toggle="dropdown" aria-haspopup="true"
											aria-expanded="false" > <span
												class="mr-2 d-none d-lg-inline text-gray-600 small">Hi, ${sessionScope.data_session.username}</span> </a>
												<!-- Dropdown - User Information -->
											<div
												class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
												aria-labelledby="userDropdown">
												<a class="dropdown-item" href="profile" style="margin: 10px; color: orange;"> <i
													class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
													Profile
												</a>
												<c:if test="${sessionScope.role.roleId == 1 }">
													<a class="dropdown-item" href="admin" style="margin: 10px; color: orange;"> <i class="fa-solid fa-folder-gear"></i>
													Admin
													</a>
												</c:if> 
												<div class="dropdown-divider"></div>
												<a class="dropdown-item" href="login" 
													 style="margin: 10px; color: orange;"> <i
													class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
													Logout
												</a>
											</div>
										</li>
									</div>
								</c:if>

							</div>
							<div class="kode-navigation">
								<ul>
									<li><a href="home">Home</a></li>
									<li><a href="aboutUs">About Us</a></li>
									<li><a href="book">Our Books</a></li>
									<li><a href="author_list">Authors</a></li>
										<c:if test="${sessionScope.data_session != null }">
											<li><a href="viewBookCase">Book Case</a></li>
										</c:if>
										<c:if test="${sessionScope.data_session == null }">
											<li><a href="login">Book Case</a></li>
										</c:if>
									<li><a href="contactUs">Contact Us</a></li>
								</ul>
								
							</div>
							<div id="kode-responsive-navigation" class="dl-menuwrapper">
								<button class="dl-trigger">Open Menu</button>
								<ul class="dl-menu">
									<li class="menu-item kode-parent-menu"><a
										href="index.html">Home</a>
										<ul class="dl-submenu">
											<li><a href="index-1.html">Home page 1</a></li>
										</ul></li>
									<li><a href="about-us.html">About Us</a></li>
									<li class="menu-item kode-parent-menu"><a
										href="books.html">Our Books</a>
										<ul class="dl-submenu">
											<li><a href="books3-sidebar.html">Book With Sidebar</a></li>
											<li><a href="books-detail.html">Book Detail</a></li>
										</ul></li>
									<li class="menu-item kode-parent-menu"><a href="blog.html">Blog</a>
										<ul class="dl-submenu">
											<li><a href="blog-2column.html">Blog 2 Column</a></li>
											<li><a href="blog-full.html">Blog Full</a></li>
											<li><a href="blog-detail.html">Blog Detail</a></li>
										</ul></li>
									<li class="menu-item kode-parent-menu last"><a
										href="authors.html">Authors</a>
										<ul class="dl-submenu">
											<li><a href="authors.html">Authors</a></li>
											<li><a href="author-detail.html">Authors Detail</a></li>
										</ul></li>
									<li class="menu-item kode-parent-menu last"><a href="#">Events</a>
										<ul class="dl-submenu">
											<li><a href="events-2column.html">Event 2 Column</a></li>
											<li><a href="events-3column.html">Event 3 Column</a></li>
											<li><a href="event-full.html">Event Single</a></li>
											<li><a href="event-detail.html">Event Detail</a></li>
										</ul></li>
									<li class="menu-item kode-parent-menu last"><a href="#">Pages</a>
										<ul class="dl-submenu">
											<li><a href="error-404.html">Error 404</a></li>
											<li><a href="coming-soon.html">Comming Soon</a></li>
											<li class="menu-item kode-parent-menu last"><a
												href="gallery-2col.html">Gallery</a>
												<ul class="dl-submenu">
													<li><a href="gallery-2col.html">Gallery 2 Col</a></li>
													<li><a href="gallery-3col.html">Gallery 3 Col</a></li>
													<li><a href="gallery-4col.html">Gallery 4 Col</a></li>
												</ul></li>
										</ul></li>
									<li class="last"><a href="contact-us.html">Contact Us</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
		<!--HEADER END-->
		
		<!--BANNER START-->
		<div class="kode-banner">
			<ul class="bxslider">
				<li><img src="Component/images/banner-1.png" alt="">
					<div class="kode-caption-2">
						<h5>Are you searching a book...?</h5>
						<h2>Biggest Library</h2>
						<p>Sed ut perspiciatis unde omnis iste natus error sit
							voluptatem accusantium dolor emque laudantium, totam rem
							aperiam.ipsam voluptatem.</p>
					</div></li>
				<li><img src="Component/images/banner-2.png" alt="">
					<div class="kode-caption-2">
						<h5>Are you searching a book...?</h5>
						<h2>Biggest Library</h2>
						<p>Sed ut perspiciatis unde omnis iste natus error sit
							voluptatem accusantium dolor emque laudantium, totam rem
							aperiam.ipsam voluptatem.</p>
					</div></li>
				<li><img src="Component/images/banner-3.png" alt="">
					<div class="kode-caption-2">
						<h5>Are you searching a book...?</h5>
						<h2>Biggest Library</h2>
						<p>Sed ut perspiciatis unde omnis iste natus error sit
							voluptatem accusantium dolor emque laudantium, totam rem
							aperiam.ipsam voluptatem.</p>
					</div></li>
			</ul>
		</div>
		<!--BANNER END-->
		<!--BUT NOW START-->
		<div class="search-section">
			<div class="container">

				<!-- Tab panes -->
				<div role="tabpanel" class="tab-pane" id="Publications">
					<div class="form-container">
						<div class="row">
							<form action="search" method="get">
							<div class="col-md-4 col-sm-4">
								<input type="text" placeholder="Search" name="search">
							</div>
							<div class="col-md-4 col-sm-4">
								<select name="item" id="cars">
									<option value="0">--Choose filter--</option>
									<option value="book_title">Book title</option>
									<option value="author_name">Author</option>
								</select>
							</div>
							<div class="col-md-4 col-sm-12">
								<button type="submit">Search</button>
							</div>
						</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--BUT NOW END-->
	<!--CONTENT START-->
	<div class="kode-content">

		<!--BOOK GUIDE SECTION START-->
		<section>
			<div class="container">
				<!--SECTION CONTENT START-->
				<div class="section-heading-1">
					<h2>Welcome to the library</h2>
					<p>Welcome to the Most Popular Library Today</p>
					<div class="kode-icon">
						<i class="fa fa-book"></i>
					</div>
				</div>
				<!--SECTION CONTENT END-->
				<div class="row">
					<div class="col-md-3 col-sm-6">
						<div class="kode-service-3">
							<i class="fa fa-gift"></i>
							<h3>
								<a href="#">Ebooks</a>
							</h3>
							<p>an electronic version of a printed book that can be read
								on a computer.</p>
							<a href="#" class="read-more">Read More</a>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="kode-service-3">
							<i class="fa fa-book"></i>
							<h3>
								<a href="#">audiobooks</a>
							</h3>
							<p>an audiocassette or CD recording of a reading of a book,
								typically a novel.</p>
							<a href="#" class="read-more">Read More</a>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="kode-service-3">
							<i class="fa fa-clone"></i>
							<h3>
								<a href="#">Magazine</a>
							</h3>
							<p>a periodical publication containing articles and
								illustrations information.</p>
							<a href="#" class="read-more">Read More</a>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="kode-service-3">
							<i class="fa fa-calculator"></i>
							<h3>
								<a href="#">Teans &amp; Kids</a>
							</h3>
							<p>the years of a person's age from 13 to 19 and are the kids
								and teens.</p>
							<a href="#" class="read-more">Read More</a>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--BOOK GUIDE SECTION END-->
		<!--TOP SELLERS SECTION START-->
		<section class="lib-categories-section">
			<div class="container">
				<!--SECTION CONTENT START-->
				<div class="section-heading-1 dark-sec">
					<h2>Our Top Categories</h2>
					<p>Here are some of the Top Categories of the Books Available.</p>
					<div class="kode-icon">
						<i class="fa fa-book"></i>
					</div>
				</div>
				<!--SECTION CONTENT END-->
				<ul class="nav nav-tabs" role="tablist">
					<c:forEach items="${listTopCategories}" var="i">
						<li role="presentation"><a href="#${i.catId}" role="tab"
							data-toggle="tab">${i.catName}</a></li>

					</c:forEach>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane fade in active" id="${a}">

						<ul >
							<li>
							<c:forEach items="${lBookByIda}" var="i">
									<div class="col-md-3 col-sm-6 best-seller-pro">
										<figure>
											<img src="${i.bookImg }" alt="">
										</figure>
										<div class="kode-text">
											<h3>${i.bookTitle}</h3>
										</div>
										<div class="kode-caption">
											<h3>${i.bookTitle}</h3>

											<p>${i.publisher}</p>
											<p class="price">${i.viewed}</p>
											<a href="#" class="add-to-cart">Add To Cart</a>
										</div>
									</div>
								</c:forEach>
								</li>
						</ul>

					</div>
					<div role="tabpanel" class="tab-pane fade " id="${b }">
						<div class="bxslider-1">


							<c:forEach items="${lBookByIdb}" var="i">
								<div class="col-md-3 col-sm-6 best-seller-pro">
									<figure>
										<img src="${i.bookImg }" alt="">
									</figure>
									<div class="kode-text">
										<h3>${i.bookTitle}</h3>
									</div>
									<div class="kode-caption">
										<h3>${i.bookTitle}</h3>

										<p>${i.publisher}</p>
										<p class="price">${i.viewed}</p>
										<a href="#" class="add-to-cart">Add To Cart</a>
									</div>
								</div>
							</c:forEach>
						</div>

					</div>
					<div role="tabpanel" class="tab-pane fade " id="${c }">
						<div class="bxslider-2">
							<!--PRODUCT GRID START-->
							<c:forEach items="${lBookByIdc}" var="i">
								<div class="col-md-3 col-sm-6 best-seller-pro">
									<figure>
										<img src="${i.bookImg }" alt="">
									</figure>
									<div class="kode-text">
										<h3>${i.bookTitle}</h3>
									</div>
									<div class="kode-caption">
										<h3>${i.bookTitle}</h3>

										<p>${i.publisher}</p>
										<p class="price">${i.viewed}</p>
										<a href="#" class="add-to-cart">Add To Cart</a>
									</div>
								</div>
							</c:forEach>
						</div>

					</div>

				</div>
			</div>
		</section>
	<!--BEST SELLER SLIDER SECTION START-->
		<section class="lib-papular-books">
			<div class="container">
				<!--SECTION CONTENT START-->
				<div class="section-heading-1">
					<h2>Most Popular Books</h2>
					<p>The Most Popular Books Today are available in Book Library</p>
					<div class="kode-icon">
						<i class="fa fa-book"></i>
					</div>
				</div>
				<div class="row">
					<!--SECTION CONTENT END-->
					<ul class="nav tab-img nav-tabs" role="tablist">
						<c:forEach items="${topBook }" var="i">
							<li role="presentation" class="col-md-4 col-sm-3"><a
								href="#${i.bookId }" role="tab" data-toggle="tab">
									<div class="lib-papular-thumb">
										<img src="${i.bookImg }" alt="">
									</div>
							</a></li>
						</c:forEach>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content content-book">
						<c:forEach items="${topBook}" var="i">
							<div role="tabpanel" class="tab-pane fade"
								id="${i.bookId }">
								<div class="lib-papular">
									<div class="kode-thumb">
										<img src="${i.bookImg }" alt="">
									</div>
									<div class="kode-text">
										<h2>${i.bookTitle }</h2>
										<h4>${i.publisher }</h4>
										<p>${i.brief}</p>
										<div class="lib-price">
											
											<a href="detail?id=${i.bookId}">See More</a>
										</div>
									</div>
								</div>
							</div>
						</c:forEach>
					</div>

				</div>
			</div>
		</section>
		<!--BEST SELLER SLIDER SECTION END-->
		<!--COUNT UP SECTION START-->
		<div class="lib-count-up-section">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-6">
						<div class="count-up">
							<span class="counter circle">21</span>
							<p>Working Year</p>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="count-up">
							<span class="counter circle">8589</span>
							<p>Books Sold</p>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="count-up">
							<span class="counter circle">458</span>
							<p>Top Author</p>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="count-up">
							<span class="counter circle">750</span>
							<p>Book Published</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--COUNT UP SECTION END-->

		<!--TOP AUTHOR START-->
		<section class="kode-lib-team-member">
			<div class="container">
				<!--SECTION CONTENT START-->
				<div class="section-heading-1">
					<h2>Our Top Authors</h2>
					<p>Here are some of the Top Authors that are available in Books
						Library</p>
					<div class="kode-icon">
						<i class="fa fa-book"></i>
					</div>
				</div>
				<!--SECTION CONTENT END-->
				<div class="lib-authors">
					<div class="social-icons">
						<ul>
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
							<li><a href="#"><i class="fa fa-skype"></i></a></li>
						</ul>
					</div>
					<img src="Component/images/lib-author.png" alt="">
					<div class="kode-caption">
						<h4>Nina Soriya</h4>
						<p>Author</p>
					</div>
				</div>
				<div class="lib-authors">
					<div class="social-icons">
						<ul>
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
							<li><a href="#"><i class="fa fa-skype"></i></a></li>
						</ul>
					</div>
					<img src="Component/images/lib-author2.png" alt="">
					<div class="kode-caption">
						<h4>Martin</h4>
						<p>Author</p>
					</div>
				</div>
				<div class="lib-authors">
					<div class="social-icons">
						<ul>
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
							<li><a href="#"><i class="fa fa-skype"></i></a></li>
						</ul>
					</div>
					<img src="Component/images/lib-author3.png" alt="">
					<div class="kode-caption">
						<h4>Alexder</h4>
						<p>Author</p>
					</div>
				</div>
				<div class="lib-authors">
					<div class="social-icons">
						<ul>
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
							<li><a href="#"><i class="fa fa-skype"></i></a></li>
						</ul>
					</div>
					<img src="Component/images/lib-author4.png" alt="">
					<div class="kode-caption">
						<h4>Jullia</h4>
						<p>Author</p>
					</div>
				</div>
			</div>
		</section>
		<!--TOP AUTHOR END-->




	</div>
	<!--CONTENT END-->
	<footer class="footer-2">
		<div class="container">
			<div class="lib-copyrights">
				<p>Copyrights © 2015-16 KodeForest. All rights reserved</p>
				<div class="social-icon">
					<ul>
						<li><a href="#" data-toggle="tooltip" title="Facebook"><i
								class="fa fa-facebook"></i></a></li>
						<li><a href="#" data-toggle="tooltip" title="Linkedin"><i
								class="fa fa-linkedin"></i></a></li>
						<li><a href="#" data-toggle="tooltip" title="Twitter"><i
								class="fa fa-twitter"></i></a></li>
						<li><a href="#" data-toggle="tooltip" title="Tumblr"><i
								class="fa fa-tumblr"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="back-to-top">
				<a href="#home"><i class="fa fa-angle-up"></i></a>
			</div>
		</div>
	</footer>
	</div>
	<!--WRAPPER END-->
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="Component/"></script>
	<script src="Component/js/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="Component/js/modernizr.custom.js"></script>
	<script src="Component/js/bootstrap.min.js"></script>
	<script src="Component/js/jquery.bxslider.min.js"></script>
	<script src="Component/js/bootstrap-slider.js"></script>
	<script src="Component/js/waypoints.min.js"></script>
	<script src="Component/js/jquery.counterup.min.js"></script>
	<script src="Component/js/owl.carousel.js"></script>
	<script src="Component/js/dl-menu/jquery.dlmenu.js"></script>
	<script type="text/javascript" src="Component/lib/hash.js"></script>
	<script type="text/javascript" src="Component/lib/booklet-lib.js"></script>
	<script src="Component/js/jquerypp.custom.js"></script>
	<script src="Component/js/jquery.bookblock.js"></script>
	<script
		src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
	<script src="Component/js/functions.js"></script>
</body>
</html>