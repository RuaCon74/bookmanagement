<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Blank</title>

    <!-- Font-Awesome -->
<script src="https://kit.fontawesome.com/db62dd06f6.js" crossorigin="anonymous"></script>

    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="Component/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="AdminDashboard.jsp">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-3">SB Admin <sup>2</sup></div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="admin">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>
            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Admin
            </div>
            <!-- Nav Item - Tables -->
            <li class="nav-item">
                <a class="nav-link" href="AdminViewBook">
                    <i class="fas fa-fw fa-table"></i>
                    <span>Views</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="AdminAddBook">
                    <i class="fa-solid fa-plus"></i>
                    <span>Add new Book</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="adminContactUs">
                   <i class="fa-solid fa-comment"></i>
                    <span>Comment</span></a>
            </li>
            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

            

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <!-- Topbar Search -->
                    <form
                        class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                        <div class="input-group">
                            
                        </div>
                    </form>

                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                        <li class="nav-item dropdown no-arrow d-sm-none">
                            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-search fa-fw"></i>
                            </a>
                            <!-- Dropdown - Messages -->
                            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                                aria-labelledby="searchDropdown">
                                <form class="form-inline mr-auto w-100 navbar-search">
                                    <div class="input-group">
                                        <input type="text" class="form-control bg-light border-0 small"
                                            placeholder="Search for..." aria-label="Search"
                                            aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button">
                                                <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>
                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Douglas McGee</span>
                                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown">
                                
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Logout
                                </a>
                            </div>
                        </li>

                    </ul>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Add Books</h1>

                </div>
                <!-- /.container-fluid -->
                
                
                	<form action="AdminAddBook" method="post">
									<div class="bg-white w-75">
										<div class="mb-3 ">
											<label for="bookId" class="fw-bold">Book Id</label><label
												class="text-danger" for="bookId">(*)</label> <input
												type="text" class="form-control" id="bookId"
												placeholder="Book id" name="bookId" required>
										</div>
										<div class="mb-3">
											<label for="bookTitle" class="fw-bold">Book Title</label><label
												class="text-danger" for="bookTitle">(*)</label> <input
												type="text" class="form-control" id="bookTitle"
												placeholder="Book Title" name="bookTitle" required>
										</div>

										<div class="mb-3">
											<label for="brief" class="fw-bold">Brief</label><label
												class="text-danger" for="brief">(*)</label> <input
												type="text" class="form-control" id="brief"
												placeholder="Brief" name="brief" required>
										</div>
										<div class="mb-3">
											<label for="publisher" class="fw-bold">Publisher</label><label class="text-danger" for="publisher">(*)</label>
											<input type="text" class="form-control" id="publisher"
												name="publisher" placeholder="Publisher" required>
										</div>
										<div class="mb-3">
											<label for="content" class="fw-bold">Content</label><label class="text-danger" for="content">(*)</label>
											<input type="text" class="form-control" id="content"
												name="content" placeholder="Content" required>
										</div>
										<div class="mb-3">
											<label for="image" class="fw-bold">Image</label><label
												class="text-danger" for="image">(*)</label> <input
												type="text" class="form-control" id="image"
												placeholder="Image" name="image" value="Component/images/"required>
										</div>
										<div class="mb-3">
											<label for="viewed" class="fw-bold">Viewed</label><label
												class="text-danger" for="email">(*)</label> <input
												type="text" class="form-control" id="viewed"
												placeholder="Viewed" name="viewed" required>
										</div>
										<div class="mb-3">
											<label for="releaseDate" class="fw-bold">Release Date</label><label
												class="text-danger" for="releaseDate">(*)</label> <input
												type="date" class="form-control" id="releaseDate"
												placeholder="Release Date" name="releaseDate" required>
										</div>
										<div class="mb-3">
											<label for="categoryId" class="fw-bold">Category Id</label><label class="text-danger" for="categoryId">(*)</label>
											<input type="text" class="form-control" id="categoryId"
												name="categoryId" required>
										</div>
										<div class="mb-3">
											<label for="authorId" class="fw-bold">Author Id</label><label class="text-danger" for="authorId">(*)</label>
											<input type="text" class="form-control" id="authorId"
												name="authorId" required>
										</div>
										
											<button type="reset" class="btn btn-warning ">
												<a class="text-decoration-none text-white"> <i
													class="fas fa-undo me-2"></i> Reset
												</a>
											</button>
											<button type="submit" class="btn btn-success ">
												<a class="text-decoration-none text-white"> <i
													class="fas fa-plus me-2"></i> Add
												</a>
											</button>
										</div>
										</form>
									</div>
								
                

            </div>
            
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Your Website 2020</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="Component/js/jquery.min.js"></script>
    <script src="Component/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="Component/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="Component/js/sb-admin-2.min.js"></script>

</body>

</html>