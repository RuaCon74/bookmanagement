package fa.training.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fa.training.dao.bookDAO;
import fa.training.dao.impl.bookDAOimpl;
import fa.training.entities.Book;

@WebServlet(name = "bookDetailController", value = "/detail")
public class bookDetailController extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String id = req.getParameter("id");
		bookDAO bDAO = new bookDAOimpl();
		Book result = bDAO.getBookById(id);
		
		List<Book> listBook = bDAO.getAllBookByCategory(result.getCategory());
		req.setAttribute("bookDetail", result);
		req.setAttribute("listBook", listBook);
		req.getRequestDispatcher("View/BookDetail.jsp").forward(req, resp);
	}
}
