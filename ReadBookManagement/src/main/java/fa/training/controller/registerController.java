package fa.training.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fa.training.dao.roleDAO;
import fa.training.dao.usersDAO;
import fa.training.dao.impl.roleDAOimpl;
import fa.training.dao.impl.usersDAOimpl;
import fa.training.entities.Users;

@WebServlet(name = "registerController", value = "/register")
public class registerController extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("View/register.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String username = req.getParameter("username");
		String email = req.getParameter("email");
		String pass = req.getParameter("pass");
		String repass = req.getParameter("repass");
		String mess = "";
		HttpSession session = req.getSession();
		usersDAO uDAO = new usersDAOimpl();
		roleDAO rDAO = new roleDAOimpl();
		List<Users> list = uDAO.getAllAccount();
		// check if the input matches the database
		for (Users users : list) { 
			if (username.equals(users.getUsername())) {
				mess += "Username is already taken \n";
			}
			// if input matches the email in the database and active = 1 => can't register
			if (email.equals(users.getEmail()) && users.getActive() == 1) {
				mess += "Email is already taken \n";
			}
		}
		if (!pass.equals(repass)) {
			mess += "Pass and re-pass not matches.";
		}
		// check if input has no error
		if (mess.equals("")) {
			uDAO.register(username, email, pass);
			Users result = uDAO.getUserByUsername(username);
			rDAO.createRole(result.getUid());
			session.setAttribute("email_session", email);
			req.getRequestDispatcher("View/verify.jsp").forward(req, resp);
		} else {
			req.setAttribute("alert", mess);
			req.getRequestDispatcher("View/register.jsp").forward(req, resp);
		}

	}
}
