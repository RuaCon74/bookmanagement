package fa.training.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fa.training.dao.authorDAO;
import fa.training.dao.bookDAO;
import fa.training.dao.impl.authorDAOimpl;
import fa.training.dao.impl.bookDAOimpl;
import fa.training.entities.Author;
import fa.training.entities.Book;

@WebServlet(name = "authorDetailController", value = "/author_detail")
public class authorDetailController extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String id=req.getParameter("index");
		String title = "Author Detail";
		
		int authorId=Integer.parseInt(id);
		authorDAO dao=new authorDAOimpl();
		bookDAO bookdao=new bookDAOimpl();
		Author a=dao.findbyId(authorId);
		List<Book> list=bookdao.findBookByAuthor(authorId);
		req.setAttribute("authorDetail", a);
		req.setAttribute("listBookOfAuthor", list);
		req.setAttribute("title", title);
		req.getRequestDispatcher("View/AuthorDetail.jsp").forward(req, resp);
	}
}
