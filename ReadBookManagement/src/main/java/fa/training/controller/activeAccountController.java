package fa.training.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fa.training.dao.usersDAO;
import fa.training.dao.impl.usersDAOimpl;

@WebServlet(name = "activeAccountController", value = "/ActiveAccount")
public class activeAccountController extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String email = req.getParameter("key");
		String token = req.getParameter("token");
		
		usersDAO uDAO = new usersDAOimpl();
		boolean result = uDAO.updateAcc(email, token);
		if (result) {
			req.getRequestDispatcher("View/login.jsp").forward(req, resp);
		} 
	}
}
