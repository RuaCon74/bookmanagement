package fa.training.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fa.training.dao.authorDAO;
import fa.training.dao.bookDAO;
import fa.training.dao.impl.authorDAOimpl;
import fa.training.dao.impl.bookDAOimpl;
import fa.training.entities.Author;
import fa.training.entities.Book;

@WebServlet(name = "searchController", value = "/search")
public class searchController extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String item = req.getParameter("item");
		String search = req.getParameter("search");
		if (item.equals("book_title")) {
			bookDAO bDAO = new bookDAOimpl();
			List<Book> list = bDAO.searchBook(search);
			req.setAttribute("listBook", list);
			req.getRequestDispatcher("View/ViewBook.jsp").forward(req, resp);
		}
		
		if (item.equals("author_name")) {
			authorDAO aDAO = new authorDAOimpl();
			List<Author> list = aDAO.searchAuthor(search);
			req.setAttribute("list", list);
			req.getRequestDispatcher("View/ViewAuthor.jsp").forward(req, resp);
		}
	}
}
