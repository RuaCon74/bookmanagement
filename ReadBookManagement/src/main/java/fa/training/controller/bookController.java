package fa.training.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fa.training.dao.authorDAO;
import fa.training.dao.bookDAO;
import fa.training.dao.categoryDAO;
import fa.training.dao.impl.authorDAOimpl;
import fa.training.dao.impl.bookDAOimpl;
import fa.training.dao.impl.categoryDAOimpl;

import fa.training.entities.Book;
import fa.training.entities.Category;

@WebServlet(name = "bookController", value = "/book")
public class bookController extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String title = "Our Book";
		
		bookDAO bDAO = new bookDAOimpl();
		List<Book> listBook = bDAO.loadBook();
		req.setAttribute("title", title);
		req.setAttribute("listBook", listBook);
		
		categoryDAO cDAO = new categoryDAOimpl();
		List<Category> listCate = cDAO.getAllCategory();
		req.setAttribute("listCate", listCate);
		
		req.getRequestDispatcher("View/ViewBook.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		bookDAO bDAO = new bookDAOimpl();
		int catId = Integer.parseInt(req.getParameter("catId"));
		List<Book> listBook = bDAO.getAllBookByCategory(catId);
		req.setAttribute("listBook", listBook);
		categoryDAO cDAO = new categoryDAOimpl();
		List<Category> listCate = cDAO.getAllCategory();
		req.setAttribute("listCate", listCate);
		req.getRequestDispatcher("View/ViewBook.jsp").forward(req, resp);
	}
}


