package fa.training.controller;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fa.training.dao.bookDAO;
import fa.training.dao.impl.bookDAOimpl;
import fa.training.entities.Book;

/**
 * Servlet implementation class AdminUpdateBook
 */
@WebServlet(name = "AdminUpdateBook", value = "/AdminUpdateBook")
public class AdminUpdateBook extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminUpdateBook() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	//	request.getRequestDispatcher("View/AdminUpdateBook.jsp").forward(request, response);
		String id = request.getParameter("BID");
		bookDAO bDAO = new bookDAOimpl();
		Book b = bDAO.getBookById(id);
		request.setAttribute("books", b);
		request.getRequestDispatcher("View/AdminUpdateBook.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String bookId = request.getParameter("bookId");
		String bookTitle = request.getParameter("bookTitle");
		String brief = request.getParameter("brief");
		String publisher = request.getParameter("publisher");
		String content = request.getParameter("content");
		String bookImg = request.getParameter("image");
		String viewed = request.getParameter("viewed");
		String releaseDate = request.getParameter("releaseDate");
		String categoryId = request.getParameter("categoryId");
		String authorId = request.getParameter("authorId");
		
		LocalDate b = LocalDate.parse(releaseDate);
		int view = Integer.parseInt(viewed);
		int category = Integer.parseInt(categoryId);
		int author = Integer.parseInt(authorId);
		bookDAO bDAO = new bookDAOimpl();
		
		Book book = new Book();
		book.setBookId(bookId);
		book.setBookImg(bookImg);
		book.setBookTitle(bookTitle);
		book.setBrief(brief);
		book.setCategory(category);
		book.setContent(content);
		book.setPublisher(publisher);
		book.setReleaseDate(b);
		book.setViewed(view);
		book.setAuthorId(authorId);
		
		bDAO.updateBookJDBC(book);
		response.sendRedirect("AdminViewBook");
	}

}
