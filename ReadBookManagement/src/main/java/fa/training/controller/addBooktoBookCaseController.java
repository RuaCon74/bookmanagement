package fa.training.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fa.training.dao.bookCaseDAO;
import fa.training.dao.containDAO;
import fa.training.dao.impl.bookCaseDAOimpl;
import fa.training.dao.impl.containDAOimpl;

@WebServlet(name = "addBooktoBookCaseController", value = "/addBookCase")
public class addBooktoBookCaseController extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
				HttpSession session = req.getSession();

		        int userId = (int)session.getAttribute("uid_session");
		        
				bookCaseDAO bcDAO = new bookCaseDAOimpl();
				if (bcDAO.findBookCasebyID(userId) == null) {
					bcDAO.createBookCase(userId);
				}
				
				String bookId = req.getParameter("bookId");
				containDAO containDAO = new containDAOimpl();
				containDAO.insertBook(userId, bookId);
				req.getRequestDispatcher("viewBookCase").forward(req, resp);
	}
}
