package fa.training.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fa.training.common.SendingEmail;
import fa.training.dao.usersDAO;
import fa.training.dao.impl.usersDAOimpl;
import fa.training.entities.Users;

@WebServlet(name = "verifyController", value = "/verify")
public class verifyController extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("View/verify.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String email = req.getParameter("email");
		
		
		usersDAO uDAO = new usersDAOimpl();
		Users user = uDAO.getAccByEmail(email);
		
		//send mail
		String subject = "Book Library Active Token";
		String mess = "Your verification link: "+"http://localhost:8080/ReadBookManagement/ActiveAccount?key="+email+"&token="+user.getHash();
		SendingEmail se = new SendingEmail(email, mess, subject, user.getHash());
		se.sendMail();
		resp.sendRedirect("https://mail.google.com/");
		//end
	}
}
