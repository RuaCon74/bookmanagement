package fa.training.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fa.training.dao.contactUsDAO;
import fa.training.dao.impl.contactUsDAOimpl;
import fa.training.entities.Contact;

@WebServlet(name = "adminContactUsController", value = "/adminContactUs")
public class adminContactUsController extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			String in = req.getParameter("index");
			String title = "ContactUs";
			
//			if (in == null) {
//				in = "1";
//			}
//
//			int index = Integer.parseInt(in);
			contactUsDAO dao = new contactUsDAOimpl();
			List<Contact> list = dao.getAllContact();
//			Long count = dao.countAll();
//			int size = 9;
//			Long endPage = count / size;
//			if (count % size != 0) {
//				endPage++;
//			}
//			List<Contact> list = dao.getAllbyPage(index);
//				req.setAttribute("endPage", endPage);
//				if (index < endPage) {
//					req.setAttribute("next", ++index);
//				} else {
//					req.setAttribute("next", index);
//				}
//				if (index > 0) {
//					req.setAttribute("pre", --index);
//				} else {
//					req.setAttribute("pre", index);
//				}
				
				req.setAttribute("list", list);
				req.setAttribute("tag", in);
				req.getRequestDispatcher("View/AdminContactUs.jsp").forward(req, resp);

			} catch (Exception e) {
				// TODO: handle exception
			}
	}
}
