package fa.training.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fa.training.dao.contactUsDAO;
import fa.training.dao.impl.contactUsDAOimpl;

import javax.mail.*;


@WebServlet(name = "ContactUsController", value = "/contactUs")
public class ContactUsController extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String title = "Contact Us";
		req.setAttribute("title", title);
		req.getRequestDispatcher("View/ContactUs.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String name = req.getParameter("name");
		String email = req.getParameter("email");
		String phone = req.getParameter("phone");
		String message = req.getParameter("message");
		
		contactUsDAO cDAO = new contactUsDAOimpl();
		boolean result = cDAO.addComments(name, email, phone, message);
		if(result == true) {
			req.setAttribute("alert", "Send Message Successful!");
			req.getRequestDispatcher("View/ContactUs.jsp").forward(req, resp);
		}
	}
	
}
