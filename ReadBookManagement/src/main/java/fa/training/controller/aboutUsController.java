package fa.training.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "aboutUsController", value = "/aboutUs")
public class aboutUsController extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String title = "About Us";
		req.setAttribute("title", title);
		req.getRequestDispatcher("View/AboutUs.jsp").forward(req, resp);
	}
}
