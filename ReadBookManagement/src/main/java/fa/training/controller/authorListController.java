package fa.training.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fa.training.dao.authorDAO;
import fa.training.dao.impl.authorDAOimpl;
import fa.training.entities.Author;

@WebServlet(name = "authorListController", value = "/author_list")
public class authorListController extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			String in = req.getParameter("index");
			String title = "Author";
			
			if (in == null) {
				in = "1";
			}

			int index = Integer.parseInt(in);
			authorDAO dao = new authorDAOimpl();
			Long count = dao.countAll();
			int size = 9;
			Long endPage = count / size;
			if (count % size != 0) {
				endPage++;
			}
			List<Author> list = dao.getAllbyPage(index);
			req.setAttribute("endPage", endPage);
			if (index < endPage) {
				req.setAttribute("next", ++index);
			} else {
				req.setAttribute("next", index);
			}
			if (index > 0) {
				req.setAttribute("pre", --index);
			} else {
				req.setAttribute("pre", index);
			}
			req.setAttribute("title", title);
			req.setAttribute("list", list);
			req.setAttribute("tag", in);
			req.getRequestDispatcher("View/ViewAuthor.jsp").forward(req, resp);

		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doPost(req, resp);
	}
}
