package fa.training.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fa.training.dao.bookDAO;
import fa.training.dao.categoryDAO;
import fa.training.dao.impl.bookDAOimpl;
import fa.training.dao.impl.categoryDAOimpl;
import fa.training.entities.Book;
import fa.training.entities.Category;

@WebServlet(name = "HomePageController", value = "/home")
public class HomePageController extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		 categoryDAO dao=new categoryDAOimpl();
		 bookDAO bookdao=new bookDAOimpl();
		 List<Book> listTopBook=bookdao.findTop6Book();
		List<Category> listTopCat=dao.findTop3Category();
		int a=listTopCat.get(0).getCatId();
		int b=listTopCat.get(1).getCatId();
		int c=listTopCat.get(2).getCatId();
		
		req.setAttribute("a",a);
		req.setAttribute("b",b);
		req.setAttribute("c",c);
		
		List<Book> lBookByIda=bookdao.findTop4BookByCategory(a);
		List<Book> lBookByIdb=bookdao.findTop4BookByCategory(b);
		List<Book> lBookByIdc=bookdao.findTop4BookByCategory(c);
		
		req.setAttribute("listTopCategories", listTopCat);
		req.setAttribute("lBookByIda",lBookByIda);
		req.setAttribute("lBookByIdb",lBookByIdb);
		req.setAttribute("lBookByIdc",lBookByIdc);
		req.setAttribute("topBook",listTopBook);
		req.getRequestDispatcher("View/HomePage.jsp").forward(req, resp);
	}
}
