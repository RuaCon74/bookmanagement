package fa.training.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fa.training.common.SendingEmail;
import fa.training.dao.usersDAO;
import fa.training.dao.impl.usersDAOimpl;

@WebServlet(name = "forgotPassController", value = "/forgotPass")
public class forgotPassController extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("View/forgot-password.jsp").forward(req, resp);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		SendingEmail mail = new SendingEmail();
		usersDAO uDAO = new usersDAOimpl();
		String email = request.getParameter("email");
            
        String newPassword = uDAO.generatePassword();
        uDAO.changePassword(email, newPassword);
		
		mail.sendChangePasswordEmail(newPassword, email);

		request.setAttribute("noti", "Please check your email to get new Password");
        request.getRequestDispatcher("View/forgot-password.jsp").forward(request, response);
	}

}
