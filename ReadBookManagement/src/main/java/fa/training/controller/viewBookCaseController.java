package fa.training.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fa.training.dao.bookCaseDAO;
import fa.training.dao.impl.bookCaseDAOimpl;
import fa.training.entities.Book;
import fa.training.entities.Users;

@WebServlet(name = "viewBookCaseController", value = "/viewBookCase")
public class viewBookCaseController extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		Users user =  (Users) session.getAttribute("data_session");
		bookCaseDAO bcDAO = new bookCaseDAOimpl();
		if (bcDAO.findBookCasebyID(user.getUid()) == null) {
			bcDAO.createBookCase(user.getUid());
		}
		
		String in = req.getParameter("index");
	    if(in == null) {
	    	in = "1";
	    }
	    int index = Integer.parseInt(in);
		Long count = bcDAO.getTotalBook(user.getUid());
		Long endPage = count/9;
		List<Book> listBook = bcDAO.viewBookbyPage(user.getUid(), index);
		if (count%9 != 0) {
			endPage++;
		}
		
		if (index < endPage) {
			req.setAttribute("next", ++index);
		} else {
			req.setAttribute("next", index);
		}
		if (index > 0) {
			req.setAttribute("pre", --index);
		} else {
			req.setAttribute("pre", index);
		}
		req.setAttribute("endPage", endPage);
		
		
		
		req.setAttribute("listBook", listBook);
		req.getRequestDispatcher("View/BookCase.jsp").forward(req, resp);
	}
}
