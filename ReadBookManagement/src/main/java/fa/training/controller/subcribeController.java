package fa.training.controller;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fa.training.common.SendingEmail;


@WebServlet(name = "subcribeController", value = "/sendMail")
public class subcribeController extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("View/ContactUs.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String mailTo = req.getParameter("email");
		String subject = "-- Book Library Online --";
		String[] parts = mailTo.split("@");
		String mess = "Hi, "+parts[0]+"\n"
				+ "	Thank for your subscribe, we will notify you when new books are available";
		SendingEmail se = new SendingEmail(mailTo, mess, subject, null);
		se.sendMail();
		req.getRequestDispatcher("View/ContactUs.jsp").forward(req, resp);
	}
}
