package fa.training.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.management.relation.RoleStatus;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fa.training.dao.roleDAO;
import fa.training.dao.usersDAO;
import fa.training.dao.impl.roleDAOimpl;
import fa.training.dao.impl.usersDAOimpl;
import fa.training.entities.Role;
import fa.training.entities.Users;

@WebServlet(name = "loginController", value = "/login")
public class loginController extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
        session.removeAttribute("data_session");
		req.getRequestDispatcher("View/login.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		String email = req.getParameter("email");
		String password = req.getParameter("password");
		usersDAO usersDao = new usersDAOimpl();
		roleDAO rDAO = new roleDAOimpl();	
		Users result = usersDao.loginByEmail(email, password);
		if(result != null) {
			if (result.getActive() == 0) {
				session.setAttribute("acc_session", result.getUsername());
				session.setAttribute("email_session", email);
				 
			}
			Role roleResult = rDAO.getRoleById(result.getUid());
			session.setAttribute("uid_session", result.getUid());
			session.setAttribute("data_session", result);
			session.setAttribute("role", roleResult);
			req.getRequestDispatcher("View/HomePage.jsp").forward(req, resp);
		}else {
			req.setAttribute("error_login", "Email or password is invalid, please input again");
			req.getRequestDispatcher("View/login.jsp").forward(req, resp);
		}
	
}
}