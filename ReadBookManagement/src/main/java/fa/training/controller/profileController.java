package fa.training.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fa.training.dao.usersDAO;
import fa.training.dao.impl.usersDAOimpl;
import fa.training.entities.Users;

@WebServlet(name = "ProfileController", value = "/profile")
public class profileController extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String title = "My Profile";
        req.setAttribute("title", title);
        
        HttpSession session = req.getSession();
        String username = (String) session.getAttribute("acc_session");
        
        usersDAO usersDao = new usersDAOimpl();
        Users result = usersDao.getUserByUsername(username);
        
        req.setAttribute("user", result);
        req.getRequestDispatcher("View/Profile.jsp").forward(req, resp);
	}
}
