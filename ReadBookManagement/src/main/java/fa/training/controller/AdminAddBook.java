package fa.training.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fa.training.dao.bookDAO;
import fa.training.dao.impl.bookDAOimpl;

/**
 * Servlet implementation class AdminAddBook
 */
@WebServlet("/AdminAddBook")
public class AdminAddBook extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminAddBook() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("View/AdminAddBook.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String bookId = request.getParameter("bookId");
		String bookTitle = request.getParameter("bookTitle");
		String brief = request.getParameter("brief");
		String publisher = request.getParameter("publisher");
		String content = request.getParameter("content");
		String bookImg = request.getParameter("image");
		String viewed = request.getParameter("viewed");    
		String releaseDate = request.getParameter("releaseDate");
		String categoryId = request.getParameter("categoryId");
		String authorId = request.getParameter("authorId");
		
		int a = Integer.parseInt(viewed);
		bookDAO bDAO = new bookDAOimpl();
		bDAO.createBookJDBC(bookId, bookTitle, brief, publisher, content, bookImg, a, releaseDate, categoryId, authorId);
		request.getRequestDispatcher("View/AdminViewBook.jsp").forward(request, response);
	}

}
