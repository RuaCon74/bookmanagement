package fa.training.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fa.training.dao.bookDAO;
import fa.training.dao.impl.bookDAOimpl;
import fa.training.entities.Book;

@WebServlet(name = "searchByAjaxController", value = "/searchBook")
public class searchByAjaxController extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String text = req.getParameter("search");
		bookDAO bDAO = new bookDAOimpl();
		List<Book> listBook = bDAO.searchBook(text);
		PrintWriter out = resp.getWriter();
		
		for (Book b : listBook) {
			out.println("<div class=\"col-md-4 col-sm-6\">\r\n"
					+ "									<div class=\"books-listing-4\">\r\n"
					+ "										<div class=\"kode-thumb\">\r\n"
					+ "											<a href=\"BookDetail.jsp\"><img src=\""+b.getBookImg()+"\" alt=\"\"></a>\r\n"
					+ "										</div>\r\n"
					+ "										<div class=\"kode-text\">\r\n"
					+ "											<h3>\r\n"
					+ "												<a href=\"BookDetail.jsp\">"+b.getBookTitle()+"</a>\r\n"
					+ "											</h3>\r\n"
					+ "											<p>"+b.getBrief()+"</p>\r\n"
					+ "										</div>\r\n"
					+ "										<a href=\"#\" class=\"add-to-cart\"><i\r\n"
					+ "											class=\"fa-solid fa-heart\"></i> Add to favorite</a>\r\n"
					+ "									</div>\r\n"
					+ "								</div>");
		}
	}
	
	
}
