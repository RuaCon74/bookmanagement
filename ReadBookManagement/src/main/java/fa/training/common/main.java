package fa.training.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;

import javax.persistence.Query;

import org.hibernate.Session;

import fa.training.entities.Users;

public class main {
	static Connection conn=null;
	static PreparedStatement ps=null;
	static Statement st=null;
	static ResultSet rs=null;
	public static void main(String[] args) {
		boolean a = createBookJDBC("abc", "abc", "abc", "abc", "abc", "abc", 1,  LocalDate.now().toString(), "1", "3");
		System.out.println(a);
	}
	
	public static boolean createBookJDBC(String bookId, String bookTitle, String brief, String publisher, String content,
			String bookImg, int viewed, String releaseDate, String categoryId, String authorId) {

		String qr = "insert into Book values(?,?,?,?,?,?,?,?,?,?)";
		try {
			conn = new DBContext().getConnection();
			ps = conn.prepareStatement(qr);
			ps.setString(1,bookId);
			ps.setString(2,bookTitle);
			ps.setString(3,brief);
			ps.setString(4,publisher);
			ps.setString(5,content);
			ps.setString(6,bookImg);
			ps.setInt(7,viewed);
			ps.setString(8,releaseDate);
			ps.setString(9,categoryId);
			ps.setString(10,authorId);
			ps.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
		
	}
}
