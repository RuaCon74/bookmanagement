package fa.training.common;

import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendingEmail {
	private String mailTo;
	private String message;
	private String subject;
	private String myHash;
	
	public SendingEmail() {
		// TODO Auto-generated constructor stub
	}

	public SendingEmail(String mailTo, String message, String subject, String myHash) {
		super();
		this.mailTo = mailTo;
		this.message = message;
		this.subject = subject;
		this.myHash = myHash;
	}

	public void sendMail() {
		String mailFrom = "longnthe140165@fpt.edu.vn";
		String password = "Long742000.";
		
		try {
			Properties pros = new Properties();
			pros.put("mail.transport.protocol", "smtps");
			pros.put("mail.smtps.host", "smtp.gmail.com");
			pros.put("mail.smtps.port", 465);
			pros.put("mail.smtps.auth", "true");
			pros.put("mail.smtps.quitwait", "false");
			
			Session session = Session.getDefaultInstance(pros);
			
			Message mess = new MimeMessage(session);
			mess.setSubject(subject);
			mess.setText(message);
			
			Address sender = new InternetAddress(mailFrom);
			Address receiver = new InternetAddress(this.mailTo);
			mess.setFrom(sender);
			mess.setRecipient(Message.RecipientType.TO, receiver);
			Transport tt = session.getTransport();
			tt.connect(mailFrom, password);
			tt.sendMessage(mess, mess.getAllRecipients());
			
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		
	}
	
	public void sendChangePasswordEmail(String newPassword, String email) {
		String mailFrom = "longnthe140165@fpt.edu.vn";
		String password = "Long742000.";
		
		try {
			Properties pros = new Properties();
			pros.put("mail.transport.protocol", "smtps");
			pros.put("mail.smtps.host", "smtp.gmail.com");
			pros.put("mail.smtps.port", 465);
			pros.put("mail.smtps.auth", "true");
			pros.put("mail.smtps.quitwait", "false");
			
			Session session = Session.getDefaultInstance(pros);
			MimeMessage message = new MimeMessage(session);
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
            message.setSubject("Book Library forgot Password");
            message.setText("Your new password is: " + newPassword + ".");
 
            // send message
            Transport tt = session.getTransport();
			tt.connect(mailFrom, password);
            tt.sendMessage(message, message.getAllRecipients());
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}
