package fa.training.entities;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Book")
public class Book {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "book_id")
	private String bookId;
	
	@Column(name = "book_title")
	private String bookTitle;
	
	@Column(name = "brief")
	private String brief;
	
	@Column(name = "publisher")
	private String publisher;
	
	@Column(name = "content")
	private String content;
	
	@Column(name = "book_img")
	private String bookImg;
	
	@Column(name = "viewed")
	private int viewed;
	
	@Column(name = "Release_Date")
	private LocalDate releaseDate;
	
	@Column(name = "category")
	private int category;
	
	@Column(name = "author_id")
	private String authorId;
	
	public Book() {
		// TODO Auto-generated constructor stub
	}

	public Book(String bookId, String bookTitle, String brief, String publisher, String content, String bookImg,
			int viewed, LocalDate releaseDate, int category, String authorId) {
		super();
		this.bookId = bookId;
		this.bookTitle = bookTitle;
		this.brief = brief;
		this.publisher = publisher;
		this.content = content;
		this.bookImg = bookImg;
		this.viewed = viewed;
		this.releaseDate = releaseDate;
		this.category = category;
		this.authorId = authorId;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public String getBrief() {
		return brief;
	}

	public void setBrief(String brief) {
		this.brief = brief;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getBookImg() {
		return bookImg;
	}

	public void setBookImg(String bookImg) {
		this.bookImg = bookImg;
	}

	public int getViewed() {
		return viewed;
	}

	public void setViewed(int viewed) {
		this.viewed = viewed;
	}

	public LocalDate getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(LocalDate releaseDate) {
		this.releaseDate = releaseDate;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public String getAuthorId() {
		return authorId;
	}

	public void setAuthorId(String authorId) {
		this.authorId = authorId;
	}

	@Override
	public String toString() {
		return "Book [bookId=" + bookId + ", bookTitle=" + bookTitle + ", brief=" + brief + ", publisher=" + publisher
				+ ", content=" + content + ", bookImg=" + bookImg + ", viewed=" + viewed + ", releaseDate="
				+ releaseDate + ", category=" + category + ", authorId=" + authorId + "]";
	}

	
	
	
}
