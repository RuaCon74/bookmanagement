package fa.training.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ContactUs")
public class Contact {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "contact_id")
	private int cId;
	@Column(name = "contact_name")
	private String cName;
	
	@Column(name = "contact_phone")
	private String cPhone;
	
	@Column(name = "contact_email")
	private String cEmail;
	
	@Column(name = "contact_mess")
	private String cMess;
	
	public Contact() {
		// TODO Auto-generated constructor stub
	}

	public Contact(int cId, String cName, String cPhone, String cEmail, String cMess) {
		super();
		this.cId = cId;
		this.cName = cName;
		this.cPhone = cPhone;
		this.cEmail = cEmail;
		this.cMess = cMess;
	}

	public int getcId() {
		return cId;
	}

	public void setcId(int cId) {
		this.cId = cId;
	}

	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}

	public String getcPhone() {
		return cPhone;
	}

	public void setcPhone(String cPhone) {
		this.cPhone = cPhone;
	}

	public String getcEmail() {
		return cEmail;
	}

	public void setcEmail(String cEmail) {
		this.cEmail = cEmail;
	}

	public String getcMess() {
		return cMess;
	}

	public void setcMess(String cMess) {
		this.cMess = cMess;
	}

	@Override
	public String toString() {
		return "Contact [cId=" + cId + ", cName=" + cName + ", cPhone=" + cPhone + ", cEmail=" + cEmail + ", cMess="
				+ cMess + "]";
	}
	
}
