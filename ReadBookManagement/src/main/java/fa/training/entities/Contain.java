package fa.training.entities;
import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Contain")
public class Contain implements Serializable{
	@Id
	@Column(name = "book_case_id")
	private int bookCaseId;
	
	@Id
	@Column(name = "book_id")
	private String bookId;
	
	@Column(name = "create_date")
	private LocalDate createDate;
	
	public Contain() {
		// TODO Auto-generated constructor stub
	}

	public Contain(int bookCaseId, String bookId, LocalDate createDate) {
		super();
		this.bookCaseId = bookCaseId;
		this.bookId = bookId;
		this.createDate = createDate;
	}

	public int getBookCaseId() {
		return bookCaseId;
	}

	public void setBookCaseId(int bookCaseId) {
		this.bookCaseId = bookCaseId;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public LocalDate getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDate createDate) {
		this.createDate = createDate;
	}

	@Override
	public String toString() {
		return "Contain [bookCaseId=" + bookCaseId + ", bookId=" + bookId + ", createDate=" + createDate + "]";
	}
	
	
}
