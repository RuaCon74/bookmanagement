package fa.training.entities;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Role")
public class Role {
	@Id
	@Column(name = "role_id")
	private int roleId;
	
	@Column(name = "authority")
	private int authority;
	
	public Role() {
		// TODO Auto-generated constructor stub
	}

	public Role(int roleId, int authority) {
		super();
		this.roleId = roleId;
		this.authority = authority;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public int getAuthority() {
		return authority;
	}

	public void setAuthority(int authority) {
		this.authority = authority;
	}

	@Override
	public String toString() {
		return "Role [roleId=" + roleId + ", authority=" + authority + "]";
	}
	
}
