package fa.training.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BookCase")
public class BookCase {
	@Id
	@Column(name = "book_case_id")
	private int bookCaseId;
	
	@Column(name = "book_case_name")
	private String bookCaseName;
	public BookCase() {
		// TODO Auto-generated constructor stub
	}
	public BookCase(int bookCaseId, String bookCaseName) {
		super();
		this.bookCaseId = bookCaseId;
		this.bookCaseName = bookCaseName;
	}
	public int getBookCaseId() {
		return bookCaseId;
	}
	public void setBookCaseId(int bookCaseId) {
		this.bookCaseId = bookCaseId;
	}
	public String getBookCaseName() {
		return bookCaseName;
	}
	public void setBookCaseName(String bookCaseName) {
		this.bookCaseName = bookCaseName;
	}
	@Override
	public String toString() {
		return "BookCase [bookCaseId=" + bookCaseId + ", bookCaseName=" + bookCaseName + "]";
	}
	
}
