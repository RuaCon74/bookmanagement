package fa.training.entities;

import java.time.LocalDate;

import javax.persistence.*;

@Entity
@Table(name = "Users")
public class Users {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int Uid;

	@Column(name = "user_name")
	private String username;

	@Column(name = "password")
	private String pass;

	@Column(name = "email")
	private String email;

	@Column(name = "hash")
	private String hash;
	
	@Column(name = "active")
	private int active;
	
	
	public Users() {
		// TODO Auto-generated constructor stub
	}


	public Users(int uid, String username, String pass, String email, String hash, int active) {
		super();
		Uid = uid;
		this.username = username;
		this.pass = pass;
		this.email = email;
		this.hash = hash;
		this.active = active;
	}


	public int getUid() {
		return Uid;
	}


	public void setUid(int uid) {
		Uid = uid;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPass() {
		return pass;
	}


	public void setPass(String pass) {
		this.pass = pass;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getHash() {
		return hash;
	}


	public void setHash(String hash) {
		this.hash = hash;
	}


	public int getActive() {
		return active;
	}


	public void setActive(int active) {
		this.active = active;
	}


	@Override
	public String toString() {
		return "Users [Uid=" + Uid + ", username=" + username + ", pass=" + pass + ", email=" + email + ", hash=" + hash
				+ ", active=" + active + "]";
	}

	
	
	

	
}
