package fa.training.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;

import fa.training.common.DBContext;
import fa.training.common.HibernateUltis;
import fa.training.dao.authorDAO;
import fa.training.entities.Author;
import fa.training.entities.Book;

public class authorDAOimpl implements authorDAO{
	static Connection conn=null;
	static PreparedStatement ps=null;
	static Statement st=null;
	static ResultSet rs=null;
	@Override
	public List<Author> searchAuthor(String search) {
		Session session = HibernateUltis.getFactory().openSession();
		Query q = session.createQuery("FROM Author WHERE author_name like :search ");
		q.setParameter("search", "%"+search+"%");
		List<Author> list = q.getResultList();
		session.close();
		return list;
	}
	
	@Override
	public Long countAll() {
		Session session = HibernateUltis.getFactory().openSession();
		Query q = session.createQuery("SELECT count(a) FROM Author a");
		Long result = (Long) q.getSingleResult();
		return result;
	}

	@Override
	public List<Author> getAllbyPage(int index) {
		try {
			conn=new DBContext().getConnection();
			String query = "select * from"
					+ "(select ROW_NUMBER() over(order by author_id asc) as r, *\r\n"
					+ "from Author) as x\r\n"
					+ "where r between ?*9-8 and ?*9";
			ps=conn.prepareStatement(query);
			ps.setInt(1, index);
			ps.setInt(2, index);
			rs=ps.executeQuery();
			List<Author> list=new ArrayList<>();
			while(rs.next()) {
				Author a=new Author(rs.getInt("author_id"),rs.getString("author_name"),rs.getString("author_Desc"),rs.getString("phone"),rs.getString("email"),rs.getString("author_img"));
				list.add(a);
			}
			return list;
			} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

	@Override
	public List<Author> findAll() {
		List<Author> list=new ArrayList<>();
		try{
			String query="Select * FROM Author";
			conn=new DBContext().getConnection();
		   st=conn.createStatement();
			
			ResultSet rs=st.executeQuery(query);
			while(rs.next()) {
				Author a=new Author(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6));
				list.add(a);
			}
			return list;
		} catch (Exception e) {
		}
		return null;
	}

	@Override
	public Author findbyId(int id) {
		Author a=new Author();
		try{
			String query="Select * FROM Author where author_id=?";
			conn=new DBContext().getConnection();
		   ps=conn.prepareStatement(query);
			ps.setInt(1, id);
			ResultSet rs=ps.executeQuery();
			while(rs.next()) {
				 a=new Author(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6));
				return a;
			}
			
		} catch (Exception e) {
		}
		return null;
	}
	
	
}
