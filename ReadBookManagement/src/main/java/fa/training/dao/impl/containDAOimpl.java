package fa.training.dao.impl;

import javax.persistence.Query;

import org.hibernate.Session;

import fa.training.common.HibernateUltis;
import fa.training.dao.containDAO;
import fa.training.entities.Contain;

public class containDAOimpl implements containDAO{

	@Override
	public boolean insertBook(int bookcase_id, String book_id) {
		try {
			Session session = HibernateUltis.getFactory().openSession();
			Contain contain = new Contain();
			contain.setBookCaseId(bookcase_id);
			contain.setBookId(book_id);
			contain.setCreateDate(java.time.LocalDate.now());
			session.getTransaction().begin();
			session.save(contain);
			session.getTransaction().commit();
			session.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean deleteBook(int bookcase_id, String book_id) {
		
		try {
			Session session = HibernateUltis.getFactory().openSession();
			Query q = session.createQuery("DELETE FROM Contain WHERE book_case_id = :bookcase_id AND book_id = :book_id");
			q.setParameter("bookcase_id", bookcase_id);
			q.setParameter("book_id", book_id);
			session.getTransaction().begin();
			q.executeUpdate();
			session.getTransaction().commit();
			session.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
