package fa.training.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Time;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;

import fa.training.common.DBContext;
import fa.training.common.HibernateUltis;
import fa.training.dao.bookCaseDAO;
import fa.training.entities.Book;
import fa.training.entities.BookCase;
import net.sourceforge.jtds.jdbc.DateTime;

public class bookCaseDAOimpl implements bookCaseDAO{

	@Override
	public boolean createBookCase(int id) {
		try {
			Session session = HibernateUltis.getFactory().openSession();
			BookCase bc = new BookCase();
			usersDAOimpl userDAO = new usersDAOimpl();
			bc.setBookCaseId(id);
			bc.setBookCaseName(userDAO.findbyId(id).getUsername());
			session.getTransaction().begin();
			session.save(bc);
			session.getTransaction().commit();
			session.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public BookCase findBookCasebyID(int id) {
		try {
			Session session = HibernateUltis.getFactory().openSession();
			Query q = session.createQuery("FROM BookCase WHERE book_case_id = :id");
			q.setParameter("id", id);
			BookCase bc = (BookCase)q.getSingleResult();
			return bc;
		} catch (Exception e) {
			return null;
		}
		
	}

	@Override
	public List<Book> viewBookCase(int bookcase_id) {
		Session session = HibernateUltis.getFactory().openSession();
		Query q = session.createQuery("FROM Contain c INNER JOIN Book b ON c.book_id = b.book_id WHERE  book_case_id = :bookcase_id");
		q.setParameter("bookcase_id", bookcase_id);
		List<Book> listBook = q.getResultList();
		return listBook;
	}


	
	@Override
	public List<Book> viewBookbyPage(int bookcase_id, int index){
		DBContext db = new DBContext();
		List<Book> listBook = new ArrayList<>();
		String query = "SELECT [Book].[book_id]\n"
				+ "      ,[book_title]\n"
				+ "      ,[brief]\n"
				+ "      ,[publisher]\n"
				+ "      ,[content]\n"
				+ "      ,[book_img]\n"
				+ "      ,[viewed]\n"
				+ "      ,[Release_Date]\n"
				+ "      ,[category]\n"
				+ "      ,[author_id]\n"
				+ "FROM Contain JOIN Book \n"
				+ "ON Contain.book_id = Book.book_id\n"
				+ "WHERE  book_case_id = ? \n"
				+ "ORDER BY [Contain].[create_date] DESC \n"
				+ "OFFSET ? ROWS \n"
				+ "FETCH NEXT 9 ROWS ONLY;" ;
		try {
			Connection conn = db.getConnection();
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setInt(1,bookcase_id);
			ps.setInt(2, (index-1)*9);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				listBook.add(new Book(rs.getString(1),
									  rs.getString(2),
									  rs.getString(3),
									  rs.getString(4),
									  rs.getString(5),
									  rs.getString(6),
									  rs.getInt(7),
									  rs.getDate(8).toLocalDate(),
									  rs.getInt(9),
									  rs.getString(10)));
			}
		} catch (Exception e) {
			
		}
		
		
		return listBook;
		
	}

	@Override
	public Long getTotalBook(int bookcase_id) {
		Session session = HibernateUltis.getFactory().openSession();
		Query q = session.createQuery("SELECT COUNT(c) FROM Contain c WHERE book_case_id = :bookcase");
		q.setParameter("bookcase", bookcase_id);
		Long result = (Long) q.getSingleResult();
		session.close();
		return result;
		
	}

}
