package fa.training.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;

import fa.training.common.DBContext;
import fa.training.common.HibernateUltis;
import fa.training.dao.categoryDAO;
import fa.training.entities.Category;

public class categoryDAOimpl implements categoryDAO{
	static Connection conn=null;
	static PreparedStatement ps=null;
	static Statement st=null;
	static ResultSet rs=null;
	@Override
	public List<Category> getAllCategory() {
		Session session = HibernateUltis.getFactory().openSession();
		Query q = session.createQuery("From Category");
		List<Category> list = q.getResultList();
		session.close();
		return list;
	}
	
	public	List<Category> findTop3Category() {
		List<Category> list=new ArrayList<>();
		try{
			String query="SELECT TOP(3)* FROM Category WHERE category_id\r\n"
					+ "IN(SELECT TOP (3) Category FROM Book Group by  category order by sum(viewed) DESC)";
			conn=new DBContext().getConnection();
		   st=conn.createStatement();
			ResultSet rs=st.executeQuery(query);
			while(rs.next()) {
				Category a=new Category(rs.getInt(1),rs.getString(2));
				list.add(a);
			}
			return list;
		} catch (Exception e) {
		}
		return null;
	}
}
