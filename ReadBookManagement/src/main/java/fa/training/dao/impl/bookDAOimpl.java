package fa.training.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

import javax.persistence.Query;
import org.hibernate.Session;

import fa.training.common.DBContext;
import fa.training.common.HibernateUltis;
import fa.training.dao.bookDAO;
import fa.training.entities.Author;
import fa.training.entities.Book;
import net.sourceforge.jtds.jdbc.DateTime;

public class bookDAOimpl implements bookDAO{
	static Connection conn=null;
	static PreparedStatement ps=null;
	static Statement st=null;
	static ResultSet rs=null;
	@Override
	public List<Book> searchBook(String search) {
		Session session = HibernateUltis.getFactory().openSession();
		Query q = session.createQuery("FROM Book WHERE book_title like :search ");
		q.setParameter("search", "%"+search+"%");
		List<Book> list = q.getResultList();
		session.close();
		return list;
	}
	
	@Override
	public List<Book> loadBook() {
		Session session = HibernateUltis.getFactory().openSession();
		Query q = session.createQuery("FROM Book");
		List<Book> list = q.getResultList();
		session.close();
		return list;
	}

	@Override
	public List<Book> findBookByAuthor(int id) {
		Session session = HibernateUltis.getFactory().openSession();
		Query q = session.createQuery("FROM Book WHERE author_id = :id");
		q.setParameter("id", id);
		List<Book> list = q.getResultList();
		return list;
	}
	
	@Override
	public List<Book> getAllBookByCategory(int cateId) {
		Session session = HibernateUltis.getFactory().openSession();
		Query q = session.createQuery("FROM Book WHERE category = :cateId");
		q.setParameter("cateId", cateId);
		List<Book> list = q.getResultList();
		session.close();
		return list;
	}

	@Override
	public Book getBookById(String id) {
		Session session = HibernateUltis.getFactory().openSession();
		Query q = session.createQuery("FROM Book WHERE bookId = :id");
		q.setParameter("id", id);
		Book list = (Book)q.getSingleResult();
		session.close();
		return list;
	}
	
	public	List<Book> findTop4BookByCategory(int id) {
		List<Book> list=new ArrayList<>();
		try{
			String query="SELECT TOP(4)* FROM Book WHERE category =? ORDER BY viewed DESC";
			conn=new DBContext().getConnection();
		   ps=conn.prepareStatement(query);
		   ps.setInt(1, id);
			
			ResultSet rs=ps.executeQuery();
			while(rs.next()) {
				Book a=new Book(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getInt(7), rs.getDate(8).toLocalDate(),rs.getInt(9),rs.getString(10));
				list.add(a);
			}
			return list;
		} catch (Exception e) {
		}
		return null;
	}
	public	List<Book> findTop6Book() {
		List<Book> list=new ArrayList<>();
		try{
			String query="SELECT TOP(6)* FROM Book ORDER BY viewed DESC";
			conn=new DBContext().getConnection();
		   st=conn.createStatement();
			ResultSet rs=st.executeQuery(query);
			while(rs.next()) {
				Book a=new Book(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getInt(7), rs.getDate(8).toLocalDate(),rs.getInt(9),rs.getString(10));
				list.add(a);
			}
			return list;
		} catch (Exception e) {
		}
		return null;
	}

	@Override
	public boolean deleteBook(String bid) {
		Session session = HibernateUltis.getFactory().openSession();
		Query q = session.createQuery("DELETE FROM Book WHERE bookId = :id");
		q.setParameter("id", bid);
		session.beginTransaction();
		q.executeUpdate();
		session.getTransaction().commit();
		session.close();
		return true;
	}

	@Override
	public boolean createBook() {
		// TODO Auto-generated method stub
		return false;
	}

	

	@Override
	public boolean createBookJDBC(String bookId, String bookTitle, String brief, String publisher, String content,
			String bookImg, int viewed, String releaseDate, String categoryId, String authorId) {

		String qr = "insert into Book values(?,?,?,?,?,?,?,?,?,?)";
		try {
			conn = new DBContext().getConnection();
			ps = conn.prepareStatement(qr);
			ps.setString(1,bookId);
			ps.setString(2,bookTitle);
			ps.setString(3,brief);
			ps.setString(4,publisher);
			ps.setString(5,content);
			ps.setString(6,bookImg);
			ps.setInt(7,viewed);
			ps.setString(8,releaseDate);
			ps.setString(9,categoryId);
			ps.setString(10,authorId);
			ps.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
		
	}

	@Override
	public void updateBookJDBC(Book b) {
		String qr = "update book set \r\n"
				+ "      [book_title]=?\r\n"
				+ "      ,[brief]=?\r\n"
				+ "      ,[publisher]=?\r\n"
				+ "      ,[content]=?\r\n"
				+ "      ,[book_img]=?\r\n"
				+ "      ,[viewed]=?\r\n"
				+ "      ,[Release_Date]=?\r\n"
				+ "      ,[category]=?\r\n"
				+ "      ,[author_id]=?\r\n"
				+ "	  Where book_id=?";
		try {
			conn = new DBContext().getConnection();
			ps = conn.prepareStatement(qr);
			ps.setString(1,b.getBookTitle());
			ps.setString(2,b.getBrief());
			ps.setString(3,b.getPublisher());
			ps.setString(4,b.getContent());
			ps.setString(5,b.getBookImg());
			ps.setInt(6,b.getViewed());
			ps.setDate(7, Date.valueOf(b.getReleaseDate()));
			ps.setInt(8,b.getCategory());
			ps.setString(9,b.getAuthorId());
			ps.setString(10,b.getBookId());
			ps.executeUpdate();
			
		} catch (Exception e) {
			
		}
		
	}

	@Override
	public boolean deleteBookContain(String bid) {
		Session session = HibernateUltis.getFactory().openSession();
		Query q = session.createQuery("DELETE FROM Contain WHERE bookId = :id");
		q.setParameter("id", bid);
		session.beginTransaction();
		q.executeUpdate();
		session.getTransaction().commit();
		session.close();
		return true;
	}

	@Override
	public List<Book> getBookByPage(int page) {
		try {
			List<Book> listBook = new ArrayList<Book>();
			Connection connection = new DBContext().getConnection();
			String sql = "SELECT * FROM Book "
					   + "ORDER BY book_id OFFSET ? ROWS FETCH NEXT 6 ROWS ONLY";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, ((page - 1) * 3));

			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				Book b = new Book();

				b.setBookId(rs.getString("book_id"));
				b.setBookTitle(rs.getString("book_title"));
				b.setBrief(rs.getString("brief"));
				b.setPublisher(rs.getString("publisher"));
				b.setContent(rs.getString("content"));
				b.setBookImg(rs.getString("book_img"));
				b.setViewed(rs.getInt("viewed"));
				b.setReleaseDate(rs.getDate("Release_Date").toLocalDate());
				b.setAuthorId(rs.getString("author_id"));
				listBook.add(b);
				return listBook;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;

		
	}
	
	
}
