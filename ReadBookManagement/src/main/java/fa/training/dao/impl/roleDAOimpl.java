package fa.training.dao.impl;

import javax.persistence.Query;

import org.hibernate.Session;

import fa.training.common.HibernateUltis;
import fa.training.dao.roleDAO;
import fa.training.entities.Role;
import fa.training.entities.Users;

public class roleDAOimpl implements roleDAO{

	@Override
	public boolean createRole(int id) {
		Session session = HibernateUltis.getFactory().openSession();
		Role r = new Role();
		r.setRoleId(id);
		r.setAuthority(0);
		session.beginTransaction();
		session.save(r);
		session.getTransaction().commit();
		session.close();
		return true;
		
	}

	@Override
	public Role getRoleById(int id) {
		Session session = HibernateUltis.getFactory().openSession();
		Query q = session.createQuery("FROM Role WHERE role_id = :id");
		q.setParameter("id", id);
		Role result = (Role)q.getSingleResult();
		return result;
	}

}
