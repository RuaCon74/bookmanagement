package fa.training.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;

import fa.training.common.DBContext;
import fa.training.common.HibernateUltis;
import fa.training.dao.contactUsDAO;
import fa.training.entities.Contact;

public class contactUsDAOimpl implements contactUsDAO{
	static Connection conn=null;
	static PreparedStatement ps=null;
	static Statement st=null;
	static ResultSet rs=null;
	@Override
	public boolean addComments(String name, String email, String phone, String mess) {
		try {
			Session session = HibernateUltis.getFactory().openSession();
			Contact contact = new Contact();
			contact.setcEmail(email);
			contact.setcName(name);
			contact.setcPhone(phone);
			contact.setcMess(mess);
			
			session.getTransaction().begin();
			session.save(contact);
			session.getTransaction().commit();
			session.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public List<Contact> getAllbyPage(int index) {
		try {
			conn=new DBContext().getConnection();
			String query = "select * from"
					+ "(select ROW_NUMBER() over(order by contact_id asc) as r, *\r\n"
					+ "from ContactUs) as x\r\n"
					+ "where r between ?*9-8 and ?*9";
			ps=conn.prepareStatement(query);
			ps.setInt(1, index);
			ps.setInt(2, index);
			rs=ps.executeQuery();
			List<Contact> list=new ArrayList<>();
			while(rs.next()) {
				Contact a=new Contact(rs.getInt("contact_id"),rs.getString("contact_name"),rs.getString("contact_phone"),rs.getString("contact_email"),rs.getString("contact_mess"));
				list.add(a);
			}
			return list;
			} catch (Exception e) {
		}
		return null;
	}

	@Override
	public List<Contact> getAllContact() {
		Session session = HibernateUltis.getFactory().openSession();
		Query q = session.createQuery("FROM Contact");
		List<Contact> list = q.getResultList();
		session.close();
		return list;
	}

	@Override
	public Long countAll() {
		Session session = HibernateUltis.getFactory().openSession();
		Query q = session.createQuery("SELECT count(a) FROM ContactUs a");
		Long result = (Long) q.getSingleResult();
		return result;
	}

}
