package fa.training.dao.impl;

import java.util.List;
import java.util.Random;

import javax.persistence.Query;

import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.Session;

import fa.training.common.HibernateUltis;
import fa.training.common.SendingEmail;
import fa.training.dao.usersDAO;
import fa.training.entities.Users;

public class usersDAOimpl implements usersDAO{

	@Override
	public List<Users> getAllAccount() {
		Session session = HibernateUltis.getFactory().openSession();
		Query q = session.createQuery("FROM Users");
		List<Users> list = q.getResultList();
		session.close();
		return list;
	}

	@Override
	public Users loginByEmail(String email, String password) {
		try {
			Session session = HibernateUltis.getFactory().openSession();
			Query q = session.createQuery("FROM Users WHERE email = :email AND password = :pass");
			q.setParameter("email", email);
			q.setParameter("pass", password);
			Users listUsers = (Users)q.getSingleResult();
			session.close();
			return listUsers;
		} catch (Exception e) {
			return null;
		}
		
	}

	@Override
	public Users findbyId(int id) {
		Session session = HibernateUltis.getFactory().openSession();
		Query q = session.createQuery("FROM Users WHERE id = :id");
		q.setParameter("id", id);
		Users u = (Users)q.getSingleResult();
		return u;
	}

	@Override
	public boolean register(String user, String email, String pass) {
		//create Hash code
		Random rd = new Random();
		rd.nextInt(999999);
		String myHash = DigestUtils.md5Hex(""+rd);
		//end

		try {
			Session session = HibernateUltis.getFactory().openSession();
			Users u = new Users();
			u.setUsername(user);
			u.setEmail(email);
			u.setPass(pass);
			u.setHash(myHash);
			session.getTransaction().begin();
			session.save(u);
			session.getTransaction().commit();
			session.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean updateAcc(String email, String hash) {
		try {
			Session session = HibernateUltis.getFactory().openSession();
			Query q = session.createQuery("UPDATE Users SET active = 1 WHERE email = :email AND hash = :hash");
			q.setParameter("email", email);
			q.setParameter("hash", hash);
			session.getTransaction().begin();
			q.executeUpdate();
			session.getTransaction().commit();
			session.close();
			return true;
		} catch (Exception e) {
			return false;
		}	
	}

	@Override
	public Users getAccByEmail(String email) {
		Session session = HibernateUltis.getFactory().openSession();
		Query q = session.createQuery("From Users WHERE email = :email");
		q.setParameter("email", email);
		Users result = (Users) q.getSingleResult();
		return result;
	}

	@Override
	public Users getUserByUsername(String name) {
		Session session = HibernateUltis.getFactory().openSession();
		Query q = session.createQuery("From Users WHERE username = :name");
		q.setParameter("name", name);
		Users result = (Users) q.getSingleResult();
		return result;
	}

	@Override
	public boolean changePassword(String email, String newPass) {
		try {
			Session session = HibernateUltis.getFactory().openSession();
			Query q = session.createQuery("UPDATE Users SET password = :password WHERE email = :email");
			q.setParameter("password", newPass);
			q.setParameter("email", email);
			session.getTransaction().begin();
			q.executeUpdate();
			session.getTransaction().commit();
			session.close();
			return true;
		} catch (Exception e) {
			return false;
		}	
	}

	@Override
	public String generatePassword() {
		int leftLimit = 97;
        int rightLimit = 122;
        int len = 10;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        String generatedString = buffer.toString();
        return generatedString;
	}

	
 
	
}
