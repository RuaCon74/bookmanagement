package fa.training.dao;

import java.util.List;

import fa.training.entities.Book;

public interface bookDAO {
	List<Book> searchBook(String search);
	List<Book> loadBook();
	List<Book> findBookByAuthor(int id);
	List<Book> getAllBookByCategory(int cateId);
	Book getBookById(String id);
	List<Book> findTop4BookByCategory(int id);
	List<Book> findTop6Book();
	boolean deleteBook(String bid);
	boolean createBook();
	boolean createBookJDBC(String bookId, String bookTitle,String brief, String publisher,String content, String bookImg,int viewed, String releaseDate,String categoryId,String authorId);
	void updateBookJDBC(Book b);
	boolean deleteBookContain(String bid);
	List<Book> getBookByPage(int page);
}
