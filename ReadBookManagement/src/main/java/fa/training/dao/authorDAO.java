package fa.training.dao;

import java.util.List;

import fa.training.entities.Author;

public interface authorDAO {
	List<Author> searchAuthor(String search);
	 List<Author> findAll();
	 Long countAll ();
	 List<Author> getAllbyPage(int index);
	 Author findbyId(int id);
}
