package fa.training.dao;

import java.util.List;

import fa.training.entities.Users;

public interface usersDAO {
	Users loginByEmail(String email, String password);
	Users findbyId(int id);
	Users getAccByEmail(String email);
	boolean register(String user, String email, String pass);
	boolean updateAcc(String email, String hash);
	List<Users> getAllAccount();
	Users getUserByUsername(String name);
	boolean changePassword(String email,String newPass);
	String generatePassword();
	
	
}
