package fa.training.dao;

public interface containDAO {
	boolean insertBook(int bookcase_id, String book_id);
	boolean deleteBook(int bookcase_id, String book_id);
}
