package fa.training.dao;

import fa.training.entities.Role;

public interface roleDAO {
	boolean createRole(int id);
	Role getRoleById(int id);
}
