package fa.training.dao;

import java.util.List;

import fa.training.entities.Contact;

public interface contactUsDAO {
	boolean addComments(String name, String email, String phone, String mess);
	List<Contact> getAllbyPage(int index);
	List<Contact> getAllContact();
	Long countAll ();
}
