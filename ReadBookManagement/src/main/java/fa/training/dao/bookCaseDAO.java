package fa.training.dao;

import java.util.List;

import fa.training.entities.Book;
import fa.training.entities.BookCase;

public interface bookCaseDAO {
	boolean createBookCase(int id);
	BookCase findBookCasebyID(int id);
	List<Book> viewBookCase(int bookcase_id);
	List<Book> viewBookbyPage(int bookcase_id, int index);
	Long getTotalBook(int bookcase_id);
}
