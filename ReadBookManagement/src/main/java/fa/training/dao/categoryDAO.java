package fa.training.dao;

import java.util.List;

import fa.training.entities.Category;

public interface categoryDAO {
	List<Category> getAllCategory();
	List<Category> findTop3Category();
}
